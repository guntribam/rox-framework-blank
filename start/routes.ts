import Route from '@ioc:Adonis/Core/Route'
import { AuthException } from 'App/_infra/Exceptions/AuthException'

Route.get('/inittestbasestate', '_infra/tests/TestController.initTestBaseState')
Route.get('/restoretestbasestate', '_infra/tests/TestController.restoreTestBaseState')
Route.get('/resettestbasestate','_infra/tests/TestController.resetTestBaseState')

Route.post('/seed','_infra/tests/TestController.seed')

Route.get('/logout', async ({ session }) => {
  session.clear()
  throw AuthException.logout();
}).prefix('api/')

Route.group(() => {
  Route.get('/userinfo', async ({auth}) => auth.user)
  Route.get('/trocaorgao', '_corporativo/TrocaOrgaoPapel/TrocaOrgaoPapelController.orgao')
  Route.get('/trocapapel', '_corporativo/TrocaOrgaoPapel/TrocaOrgaoPapelController.papel')
}).prefix('api/').middleware('auth')


Route.on('*').redirectToPath('/')

