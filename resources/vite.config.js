import { defineConfig } from "vite";
import {svelte} from "@sveltejs/vite-plugin-svelte";
import tsconfigPaths from 'vite-tsconfig-paths'
import replace from '@rollup/plugin-replace';

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  const isProduction = mode === "production";
  return {
    plugins: [
      svelte(),
      replace({
        '_API_': isProduction ? '/api' : 'http://localhost:3333/api',
      }),
      tsconfigPaths(),
    ],
    optimizeDeps: {
      exclude: ["@roxi/routify"]
    },
    build: {
      minify: isProduction,
      brotliSize: true, // To Speed Up Build
      rollupOptions: {
        output: {format: "esm", dir: '../public'}
      }
    },
  };
});

