import { writable } from 'svelte/store'

export const toasts = writable([])

let pauseToasts = false
let timeouts = {}

export const pause = () => {
  pauseToasts = true
}

export const resume = (id) => {
  pauseToasts = false
  timeouts[id]()
}

export const dismissToast = (id) => {
  if (!pauseToasts) {
    toasts.update((all) => all.filter((t: any) => t.id !== id))
  }
}

export const closeToast = (id) => {
  toasts.update((all) => all.filter((t: any) => t.id !== id))
}

export interface Toast {
  message: string
  timeout?: number
  type?: 'error' | 'success'
}

export const addToast = (toast: Toast) => {
  const id = Math.floor(Math.random() * 10000)

  const defaults = {
    id,
    type: 'error',
    timeout: 1500,
    dismissible: true,
  }

  toasts.update((all: any) => [{ ...defaults, ...toast }, ...all] as any)

  timeouts[id] = () => setTimeout(() => dismissToast(id), toast.timeout ?? defaults.timeout)
  timeouts[id]()
}
