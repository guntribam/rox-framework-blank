import { writable } from "svelte/store";

function sidebarStore() {
    const { subscribe, set } = writable(true);

    return {
        subscribe,
        open: () => set(true),
        close: () => set(false),
    };
}
export const SidebarStore = sidebarStore();