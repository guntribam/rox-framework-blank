import { writable } from "svelte/store";
import type {UserInfo} from '@types'

function userInfoStore() {

    const { subscribe, set} = writable<Partial<UserInfo>>({});

    return { subscribe, set };
}
export const userInfo = userInfoStore();
