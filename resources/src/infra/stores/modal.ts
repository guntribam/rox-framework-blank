import { writable } from 'svelte/store';
const ModalStore = {
    modalId: writable(1)
}
export { ModalStore }