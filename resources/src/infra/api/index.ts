import { addToast, userInfo } from '@stores'
import type { GridModel, RequestParams } from '@types'
import type { Writable } from 'svelte/store'

const API = '_API_'

interface GET {
  resource: string
  queryParams: string
}
const GET = async ({ resource, queryParams }: GET): Promise<any> => {
  const response = await fetch(`${API}/${resource}${queryParams}`, {
    credentials: 'include',
  })
  return await response.json()
}

interface GET_GRIDMODEL {
  resource: string
  requestParams: RequestParams
}
const GET_GRIDMODEL = async ({ resource, requestParams }: GET_GRIDMODEL): Promise<GridModel> => {
  const { page = 1, sort, limit = 10, load = [], filter = {} } = requestParams
  let _page = `?page=${page}`
  let _limit = `&limit=${limit}`
  let _filter = `&filter=${JSON.stringify(filter)}`
  let _load = `&load=${JSON.stringify(load)}`
  let _sort = `&sort=[${sort
    .map((s) => `{"column": "${s.column}", "order": "${s.order}"}`)
    .join(',')}]`
  let queryParams = `${_page}${_limit}${_filter}${_load}${_sort}`
  const result = await GET({ resource, queryParams })
  return result
}

interface GET_BY_ID {
  resource: string
  load: string[]
  filter: any
}
const GET_BY_ID = async ({ resource, load = [], filter }: GET_BY_ID): Promise<any> => {
  let _filter = `?filter=${JSON.stringify(filter)}`
  let _load = `&load=${JSON.stringify(load)}`
  return await GET({ resource, queryParams: `${_filter}${_load}` })
}
interface GET_ALL {
  resource: string
}
const GET_ALL = async ({ resource }: GET_ALL): Promise<any> => {
  const response = await await fetch(`${API}/${resource}`, {
    credentials: 'include',
  })
  return await response.json()
}

interface POST {
  resource: string
  body?: any
}
const POST = async ({ resource, body }: POST) => {
  const response = await fetch(`${API}/${resource}`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    credentials: 'include',
    body: JSON.stringify(body),
  })
  return await response.json()
}

interface PUT {
  resource: string
  id: number
  body?: any
}
const PUT = async ({ resource, id, body }: PUT) => {
  const response = await fetch(`${API}/${resource}/${id}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
    },
    credentials: 'include',
    body: JSON.stringify(body),
  })
  return await response.json()
}

interface DELETE {
  resource: string
  ids: number[]
}
const DELETE = async ({ resource, ids }: DELETE) => {
  const response = await fetch(`${API}/${resource}/${ids.join(',')}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
    },
    credentials: 'include',
  })
  return response.json()
}

interface Params {
  resource: string
  queryParams: string
  requestParams: RequestParams
  body?: any
  id: number
  ids: number[]
  load: any
  filter: any
}

const requestBuilder = async (
  req: RequestAttrs & { params: any; method: (params: Params) => Promise<any> }
) => {
  req.loading && req.loading.set(true)
  const response = await req.method(req.params)
  console.log({ response })
  req.loading && req.loading.set(false)
  if (response.errors) {
    req.errors && req.errors.set(response.errors)
    req.errorToast
      ? addToast(req.errorToast)
      : response.errors.forEach(({ message }) => addToast({ message }))
    req.onError && (await req.onError())
  } else {
    req.successToast && addToast(req.successToast)
    req.onSuccess && (await req.onSuccess(response))
  }
  return response
}

interface RequestAttrs {
  loading?: Writable<boolean>
  errorToast?: any
  successToast?: any
  errors?: Writable<any>
  formLabels?: any
  onError?: () => Promise<void>
  onSuccess?: (response: any) => Promise<void>
}

type _GET = { params: Pick<Params, 'resource' | 'queryParams'> } & RequestAttrs
type _GET_ALL = { params: Pick<Params, 'resource'> } & RequestAttrs
type _GET_GRIDMODEL = { params: Pick<Params, 'resource' | 'requestParams'> } & RequestAttrs
type _GET_BY_ID = {
  params: Pick<Params, 'resource'> & Pick<RequestParams, 'load' | 'filter'>
} & RequestAttrs
type _POST = { params: Pick<Params, 'resource' | 'body'> } & RequestAttrs
type _PUT = { params: Pick<Params, 'resource' | 'id' | 'body'> } & RequestAttrs
type _DELETE = { params: Pick<Params, 'resource' | 'ids'> } & RequestAttrs


export const request = {
  GET: (req: _GET) => requestBuilder({ ...req, method: GET }),
  GET_ALL: (req: _GET_ALL) => requestBuilder({ ...req, method: GET_ALL }),
  GET_GRIDMODEL: (req: _GET_GRIDMODEL) => requestBuilder({ ...req, method: GET_GRIDMODEL }),
  GET_BY_ID: (req: _GET_BY_ID) => requestBuilder({ ...req, method: GET_BY_ID }),
  POST: (req: _POST) => requestBuilder({ ...req, method: POST }),
  PUT: (req: _PUT) => requestBuilder({ ...req, method: PUT }),
  DELETE: (req: _DELETE) => requestBuilder({ ...req, method: DELETE }),
  LOGIN: async function () {
    try {
      const response = await fetch(`${API}/userinfo`, { credentials: 'include' })
      const user = await response.json()
      userInfo.set(user)
    } catch (error) {
      console.log('Login error:', error)
    }
  },
  LOGOUT: async function () {
    try {
      await fetch(`${API}/logout`, { credentials: 'include' })
      userInfo.set({})
    } catch (error) {
      console.log('Logout error:', error)
    }
    await this.LOGIN()
    location.reload()
  },
}
