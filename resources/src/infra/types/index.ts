import type { Writable } from 'svelte/store'
export * from '../../../../shared'

interface ExternalConsole{
  console: any,
  onCreate?: (state: ConsoleState<any>) => Promise<any>
  onUpdate?: (state: ConsoleState<any>) => Promise<any>
  onDelete?: (state: ConsoleState<any>) => Promise<any>
}

export interface ContextProps<T> {
  isCreate: Writable<boolean>
  isUpdate: Writable<boolean>
  formErrors: Writable<any>
  formLabels: Writable<any>
  model: Writable<Partial<T>>
}

export interface ConsoleState<T> {
  resource: string
  reference: any
  actionsSuffix: string,
  consoles: ExternalConsole[],
  loadingGrid: Writable<boolean>
  loadingChooser: Writable<boolean>
  loadingCreateForm: Writable<boolean>
  loadingUpdateForm: Writable<boolean>
  loadingDeleteForm: Writable<boolean>
  showCreateModal: Writable<boolean>
  showUpdateModal: Writable<boolean>
  showDeleteModal: Writable<boolean>
  formErrors: Writable<any>
  modelToDelete: Writable<number>
  modelsToDeleteMany: Writable<number[]>
  result: any
  errors: Writable<Array<any>>
  model: Writable<Partial<T>>
  modelType: Partial<T>
  filter: Writable<Partial<T>>
  gridModel: Writable<GridModel>
  requestParams: Writable<RequestParams>
  fields: Writable<Field[]>
  formLabels: Writable<any>,
  consoleLabel: string,
  picker?: boolean
  refreshGrid: () => Promise<void>
  contextProps: () => ContextProps<T>,
  createActionHandlers: () => any[]
  getActions: () => any
  create: (model: any) => Promise<void>
  update: (model: any) => Promise<void>
  destroy: (ids: number[]) => Promise<void>
  getGridModel: (params: RequestParams) => Promise<void>
  getById: (id: number) => Promise<void>
  selectForUpdate: (entity: any) => void
  selectForDelete: (ids: number[]) => void
  onCreateOpen: () => void
  onUpdateOpen: () => void
  onDeleteOpen: () => void
  onCreateClose: () => void
  onUpdateClose: () => void
  onDeleteClose: () => void
  reset: () => void
}

export type Actions = Array<'detail' | 'update' | 'delete'>

export interface ChooserEntity {
  entityId: string
  fieldToShow: string
  entities: Array<any>
}
export interface ModelInput<ErrorType extends InputError> {
  model: any
  error: ErrorType
}

export interface ErrorProp {
  msg?: string
  isInvalid: boolean
}

export interface InputError {}

export interface RequiredError extends InputError {
  req: ErrorProp
}
export interface InclusionError extends InputError {
  inclusion: ErrorProp
}
export interface LengthError extends InputError {
  maxLength: ErrorProp
  minLength: ErrorProp
}
export interface DateError extends InputError {
  maxDate: ErrorProp
  minDate: ErrorProp
}
export interface NumberError extends InputError {
  maxNumber: ErrorProp
  minNumber: ErrorProp
}

export interface Option {
  label: string
  value: object
}

export interface Field {
  label: string
  name: any
  aligner?: 'left' | 'right' | 'center'
  transformer?: (field: any, row?: any) => any
  sortOrder?: 'asc' | 'desc' | 'none'
  sortName?: string
  truncate?: boolean
}

export interface ActionButtonOptions {
  onClick?: (event: any) => any
  noHover?: boolean
}

export interface ActionButtonsOptions {
  left?: ActionButtonOptions
  center?: ActionButtonOptions
  right?: ActionButtonOptions
}

export interface GridModel {
  meta: {
    currentPage: number
    firstPage: number
    lastPage: number
    perPage: number
    total: number
  }
  data: Array<any>
}

export interface Sort {
  column: string
  order: 'asc' | 'desc'
}

export interface RequestParams {
  page: number
  limit: number
  sort: Array<Sort>
  filter: any
  load: Array<string>
}

export type GridRequest = (resource: string, params: RequestParams) => GridModel
