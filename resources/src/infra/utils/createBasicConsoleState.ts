import { get, writable } from 'svelte/store'
import { request } from '@api'
import type { RequestParams, ConsoleState, Field } from '@types'
import { userInfo } from '@stores'

export const createBasicConsoleState = <T>(): ConsoleState<T> => ({
  resource: 'noResource',
  reference: null,
  consoles: [],
  loadingCreateForm: writable(false),
  loadingUpdateForm: writable(false),
  loadingDeleteForm: writable(false),
  loadingGrid: writable(false),
  loadingChooser: writable(false),
  result: writable({}),
  errors: writable([]),
  model: writable({}),
  modelType: {},
  modelToDelete: writable(0),
  modelsToDeleteMany: writable([]),
  fields: writable<Field[]>([]),
  formLabels: writable({}),
  consoleLabel: '',
  showCreateModal: writable(false),
  showUpdateModal: writable(false),
  showDeleteModal: writable(false),
  formErrors: writable({}),
  gridModel: writable({
    meta: {
      currentPage: 1,
      firstPage: 1,
      lastPage: 1,
      perPage: 10,
      total: 0,
    },
    data: [],
  }),
  requestParams: writable({
    page: 1,
    limit: 10,
    sort: [{ column: 'id', order: 'asc' }],
    filter: {},
    load: [],
  }),
  filter: writable({}),
  refreshGrid: async function () {
    await this.getGridModel(get(this.requestParams))
  },
  contextProps: function () {
    const model = this.model
    const isCreate = this.showCreateModal
    const isUpdate = this.showUpdateModal
    const formErrors = this.formErrors
    const formLabels = this.formLabels
    return { isCreate, isUpdate, formErrors, formLabels, model }
  },
  actionsSuffix: '',
  createActionHandlers: function () {
    return [
      { name: 'CADASTRAR', isOnGrid: false, handler: () => this.onCreateOpen() },
      { name: 'EXCLUIR', isOnGrid: false, handler: (row) => this.selectForDelete([row.id]) },
      {
        name: 'ALTERAR',
        icon: 'Pencil',
        isOnGrid: true,
        handler: (row) => this.selectForUpdate(row),
      },
      {
        name: 'EXCLUIR',
        icon: 'Trash',
        isOnGrid: true,
        handler: (row) => this.selectForDelete([row.id]),
      },
    ]
  },
  getActions: function () {
    const acoes: any = get(userInfo)?.acessoAtual?.acoes
      .filter((a) => a.nome.includes(this.actionsSuffix))
      .map((a) => a.nome.replace(`_${this.actionsSuffix}`, ''))
    const result = acoes.map((action) => this.createActionHandlers().filter((prop) => prop.name === action)).flatMap(a => a)
    return result
  },
  getGridModel: async function (requestParams: RequestParams) {
    await request.GET_GRIDMODEL({
      params: { resource: this.resource, requestParams },
      loading: this.loadingGrid,
      errors: this.errors,
      onSuccess: async (response) => this.gridModel.set(response),
    })
  },
  getById: async function (id: number): Promise<void> {
    const entity = await request.GET_BY_ID({
      params: {
        resource: this.resource,
        load: get<RequestParams>(this.requestParams).load,
        filter: { id },
      },
      loading: this.loadingChooser,
      errors: this.errors,
    })
    return entity.data[0]
  },
  create: async function(model: any, message?: string): Promise<void>{
    await request.POST({
      params: { resource: this.resource, body: model },
      loading: this.loadingCreateForm,
      errors: this.errors,
      formLabels: get(this.formLabels),
      onSuccess: async (response: any) => {
        this.result.set(response.data)
        this.showCreateModal.set(false)
        this.reset()
        await this.refreshGrid()
      },
      ...(message && { successToast: { message } }),
    })
  },
  update: async function (model: any, message?: string): Promise<void> {
    await request.PUT({
      params: { resource: this.resource, id: model.id, body: model },
      loading: this.loadingUpdateForm,
      errors: this.errors,
      formLabels: get(this.formLabels),
      onSuccess: async (response: any) => {
        this.result.set(response.data)
        this.showUpdateModal.set(false)
        this.reset()
        await this.refreshGrid()
      },
      ...(message && { successToast: { message } }),
    })
  },
  destroy: async function (ids: number[], message?: string): Promise<void> {
    console.log({ids})
    await request.DELETE({
      params: { resource: this.resource, ids },
      loading: this.loadingDeleteForm,
      errors: this.errors,
      onSuccess: async () => {
        this.showDeleteModal.set(false)
        this.reset()
        await this.refreshGrid()
      },
      ...(message && { successToast: { message } }),
    })
  },
  selectForUpdate: function (entity: any): void {
    console.log("this", this, entity)
    this.onUpdateOpen()
    this.model.set(entity)
  },
  selectForDelete: function (ids: number[]): void {
    this.onDeleteOpen()
    this.modelToDelete.set(ids)
    console.log("modelToDelete", get(this.modelToDelete))
  },
  onCreateOpen: function (): void {
    this.reset()
    this.showCreateModal.set(true)
  },
  onUpdateOpen: function (): void {
    this.reset()
    this.showUpdateModal.set(true)
  },
  onDeleteOpen: function (): void {
    this.reset()
    this.showDeleteModal.set(true)
  },
  onCreateClose: function (): void {
    this.reset()
    this.showCreateModal.set(false)
  },
  onUpdateClose: function (): void {
    this.reset()
    this.showUpdateModal.set(false)
  },
  onDeleteClose: function (): void {
    this.reset()
    this.showDeleteModal.set(false)
  },
  reset: function (): void {
    this.model.set({})
    this.modelToDelete.set(0)
    this.errors.set([])
    this.result.set(null)
    this.afterCreate = async () => {}
  },
})
