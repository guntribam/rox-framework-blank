import { DateTime } from 'luxon'
export const ddMMyyyy = (field: any) => DateTime.fromISO(field).toFormat('dd/MM/yyyy')
export const money = (field: any) => {
  return new Intl.NumberFormat('pt-BR', {
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
  }).format(field)
}
