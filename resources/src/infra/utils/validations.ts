import { DateTime } from "luxon";
import type { DateFormat, ErrorProp } from "@types";

let isInvalid = true
export const valueNotIncluded = (value: string, options: Array<any>): ErrorProp => {
    return (value && !options.some(o => o === value)) ? { msg: 'Selecione um valor das opções', isInvalid } : { isInvalid: false }
}
export const isRequired = (value: string, req: boolean): ErrorProp => {
    return (req && (value === undefined || value.length === 0)) ? { msg: 'Campo obrigatório', isInvalid } : { isInvalid: false }
}
export const isMaxLength = (value: string, max?: number): ErrorProp => {
    return (max && value?.length > max) ? { msg: `Tamanho máximo: ${max}`, isInvalid } : { isInvalid: false }
}
export const isMinLength = (value: string, min?: number): ErrorProp => {
    return (min && value?.length < min) ? { msg: `Tamanho mínimo: ${min}`, isInvalid } : { isInvalid: false }
}
export const isMaxNumber = (value: number, max?: number): ErrorProp => {
    const result = (max && value > max) ? { msg: `Tamanho máximo: ${new Intl.NumberFormat('pt-BR').format(max)}`, isInvalid } : { isInvalid: false };
    return result
}
export const isMinNumber = (value: number, min?: number): ErrorProp => {
    return (min && value < min) ? { msg: `Tamanho mínimo: ${new Intl.NumberFormat('pt-BR').format(min)}`, isInvalid } : { isInvalid: false };
}

export const isMaxDate = (value: string, max?: DateFormat): ErrorProp => {
    if (max && value) {
        const maxDate = DateTime.fromFormat(max, 'yyyy-MM-dd');
        return (DateTime.fromFormat(value, 'yyyy-MM-dd') > maxDate) ? { msg: `Data máxima: ${maxDate.toFormat('dd/MM/yyyy')}`, isInvalid } : { isInvalid: false }
    }
    return { isInvalid: false }

}
export const isMinDate = (value: string, min?: DateFormat): ErrorProp => {
    if (min && value) {
        const minDate = DateTime.fromFormat(min, 'yyyy-MM-dd');
        return (DateTime.fromFormat(value, 'yyyy-MM-dd') < minDate) ? { msg: `Data mínima: ${minDate.toFormat('dd/MM/yyyy')}`, isInvalid } : { isInvalid: false }
    }
    return { isInvalid: false }
}