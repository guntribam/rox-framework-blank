import { column, BaseModel, belongsTo, BelongsTo, manyToMany, ManyToMany, hasMany, HasMany} from '@ioc:Adonis/Lucid/Orm'
import { SimNaoConverter } from 'App/_infra/Converters'
import Acesso from '../acesso/Acesso'
import Circunscricao from '../circunscricao/Circunscricao'
import Pessoa from '../pessoa/Pessoa'
import Env from '@ioc:Adonis/Core/Env'

export default class Orgao extends BaseModel {
  static table = `${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ORGAO`

  @column({ isPrimary: true, columnName: 'iCodigo' })
  id: number

  @column({ columnName: 'sNome' })
  nome: string

  @column({ columnName: 'sNomeExtendido' })
  nomeExtendido: string

  @column({ columnName: 'sSigla' })
  sigla: string

  @column({ columnName: 'cEstatistica', ...SimNaoConverter })
  temEstatistica: boolean

  @column({ columnName: 'cAmbito' })
  ambito: string

  @column({ columnName: 'cAtividade' })
  atividade: string

  @column({ columnName: 'cStatus' })
  status: string

  @column({ columnName: 'iCircunscricao' })
  idCircunscricao: number

  @column({ columnName: 'iFiltro' })
  filtro: number

  @column({ columnName: 'msrepl_tran_version' })
  msreplTranVersion: string

  @belongsTo(() => Circunscricao, {
    foreignKey: "idCircunscricao",
    localKey: "id"
  })
  circunscricao: BelongsTo<typeof Circunscricao>;

  @manyToMany(() => Pessoa, {
    localKey: 'id',
    pivotForeignKey: 'iOrgao',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'iPessoa',
    pivotTable: `${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.R_LOTACAO`,
    pivotColumns: ['dtInicio', 'dtFim']
  })
  pessoas: ManyToMany<typeof Pessoa>

  @hasMany(() => Acesso, {
    foreignKey: "idOrgao",
    localKey: "id"
  })
  acessos: HasMany<typeof Acesso>

}
