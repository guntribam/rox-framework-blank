import { BaseModel, BelongsTo, belongsTo, column } from '@ioc:Adonis/Lucid/Orm'
import Sistema from '../sistema/Sistema'
import Env from '@ioc:Adonis/Core/Env'

export default class PapelAcesso extends BaseModel {
  static table = `${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_ACESSO`

  @column({ isPrimary: true, columnName: 'idPapelAcesso' })
  id: number

  @column({ columnName: 'nmPapelAcesso' })
  nome: string

  @column({ columnName: 'idSistema' })
  idSistema: number

  @belongsTo(() => Sistema, {
    foreignKey: 'idSistema',
    localKey: 'id',
  })
  sistema: BelongsTo<typeof Sistema>
}
