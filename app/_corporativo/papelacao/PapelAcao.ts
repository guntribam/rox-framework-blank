import { BaseModel, BelongsTo, belongsTo, column } from '@ioc:Adonis/Lucid/Orm'
import { SimNaoConverter } from 'App/_infra/Converters'
import Acao from '../acao/Acao'
import PapelAcesso from '../papelacesso/PapelAcesso'
import Sistema from '../sistema/Sistema'
import Env from '@ioc:Adonis/Core/Env'

export default class PapelAcao extends BaseModel {
  static table = `${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_ACAO`

  @column({ isPrimary: true, columnName: 'idPapelAcao' })
  id: number

  @column({ columnName: 'idSistema' })
  idSistema: number

  @column({ columnName: 'idPapelAcesso' })
  idPapelAcesso: number

  @column({ columnName: 'idAcao' })
  idAcao: number

  @column({ columnName: 'stAtivo', ...SimNaoConverter })
  ativo: boolean

  @belongsTo(() => Sistema, {
    foreignKey: 'idSistema',
    localKey: 'id',
  })
  sistema: BelongsTo<typeof Sistema>

  @belongsTo(() => PapelAcesso, {
    foreignKey: 'idPapelAcesso',
    localKey: 'id',
  })
  papelAcesso: BelongsTo<typeof PapelAcesso>

  @belongsTo(() => Acao, {
    foreignKey: 'idAcao',
    localKey: 'id',
  })
  acao: BelongsTo<typeof Acao>
}
