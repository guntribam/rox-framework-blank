import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'
import Env from '@ioc:Adonis/Core/Env'

export default class TipoSigilo extends BaseModel {
  static table = `${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.TIPO_SIGILO`

  @column({ isPrimary: true, columnName: 'idTipoSigilo' })
  id: number

  @column({ columnName: 'nmTipoSigilo' })
  nome: number

}
