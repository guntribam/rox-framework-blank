import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import TrocaOrgaoPapelException from 'App/_infra/Exceptions/TrocaOrgaoPapelException'

export default class TrocaOrgaoPapelController {
  async orgao({ auth, session, request }: HttpContextContract) {
    const user = session.get('user')
    const idOrgao = Number(request.input('idOrgao'))
    user.acessoAtual = user.acesso.find((a) => a.orgao.id === idOrgao)
    if (!user.acessoAtual) {
      throw TrocaOrgaoPapelException.orgao()
    }
    user.acessoAtual.papelAtual = user.acessoAtual.papeis[0]
    session.forget('user')
    session.put('user', user)
    await auth.check()
    return auth.user
  }

  async papel({ auth, session, request }: HttpContextContract) {
    const user = session.get('user')
    const idPapel = Number(request.input('idPapel'))
    user.acessoAtual.papelAtual = user.acessoAtual.papeis.find((p) => p.id === idPapel)
    if (!user.acessoAtual.papelAtual) {
      throw TrocaOrgaoPapelException.perfil()
    }
    session.forget('user')
    session.put('user', user)
    await auth.check()
    return auth.user
  }
}
