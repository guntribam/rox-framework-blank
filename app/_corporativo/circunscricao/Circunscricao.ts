import { column, BaseModel } from '@ioc:Adonis/Lucid/Orm'
import Env from '@ioc:Adonis/Core/Env'

export default class Circunscricao extends BaseModel {
  static table = `${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.T_CIRCUNSCRICAO`

  @column({ isPrimary: true, columnName: 'iCodigo' })
  id: number

  @column({ columnName: 'sNome' })
  nome: string

  @column({ columnName: 'sSigla' })
  sigla: string
}
