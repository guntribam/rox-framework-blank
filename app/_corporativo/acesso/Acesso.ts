import { column, BaseModel, manyToMany, ManyToMany, belongsTo, BelongsTo } from '@ioc:Adonis/Lucid/Orm'
import { DateTime } from 'luxon'
import Orgao from '../orgao/Orgao'
import Pessoa from '../pessoa/Pessoa'
import Sistema from '../sistema/Sistema'
import Env from '@ioc:Adonis/Core/Env'

export default class Acesso extends BaseModel {
  static table = `${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.R_ACESSO`

  @column({ isPrimary: true, columnName: 'iCodigo' })
  id: number

  @column({ columnName: 'iPessoa' })
  idPessoa: number

  @column({ columnName: 'iOrgao' })
  idOrgao: number

  @column({ columnName: 'iSistema' })
  idSistema: number

  @column.date({columnName: 'dtModificacao'})
  dataModificacao: DateTime

  @column({ columnName: 'iLocalidade' })
  localidade: number

  @manyToMany(() => Pessoa, {
    localKey: 'id',
    pivotForeignKey: 'iGrupo',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'iUsuario',
    pivotTable: `${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.R_GRUPO_USUARIO`,
  })
  usuarios: ManyToMany<typeof Pessoa>

  @manyToMany(() => Sistema, {
    localKey: 'id',
    pivotForeignKey: 'iGrupo',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'iSistema',
    pivotTable: `${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.R_GRUPO_SISTEMA`,
  })
  sistemas: ManyToMany<typeof Sistema>

  @belongsTo(() => Orgao, {
    foreignKey: "idOrgao",
    localKey: "id"
  })
  orgao: BelongsTo<typeof Orgao>

}
