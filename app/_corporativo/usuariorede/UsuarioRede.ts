import { column, BaseModel, hasOne, HasOne } from '@ioc:Adonis/Lucid/Orm'
import Pessoa from '../pessoa/Pessoa'
import Env from '@ioc:Adonis/Core/Env'

export default class UsuarioRede extends BaseModel {
  static table = `${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.USUARIO_REDE`

  @column({ isPrimary: true, columnName: 'iRID' })
  public id: number

  @column({ columnName: 'sLogin' })
  public login: string

  @column({ columnName: 'sEmail' })
  public email: string

  @hasOne(() => Pessoa, {foreignKey: "idUsuarioRede"})
  pessoa: HasOne<typeof Pessoa>
}
