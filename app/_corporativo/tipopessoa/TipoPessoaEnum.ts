export default {
  CEDIDO: { id: 1, descricao: 'Cedido', ativo: 'I' },
  DEMITIDO: { id: 2, descricao: 'Demitido', ativo: 'I' },
  EXONERADO: { id: 3, descricao: 'Exonerado', ativo: 'I' },
  MEMBRO_ATIVO: { id: 4, descricao: 'Membro - Ativo', ativo: 'A' },
  MEMBRO_INATIVO: { id: 5, descricao: 'Membro - Inativo', ativo: 'I' },
  QUADRO_PERMANENTE: { id: 6, descricao: 'Quadro Permanente', ativo: 'A' },
}
