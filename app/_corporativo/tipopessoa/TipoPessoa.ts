import { column, BaseModel } from '@ioc:Adonis/Lucid/Orm'
import Env from '@ioc:Adonis/Core/Env'

export default class TipoPessoa extends BaseModel {
  static table = `${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.T_TIPO_PESSOA`

  @column({ isPrimary: true, columnName: 'iCodigo' })
  public id: number

  @column({ columnName: 'sDescricao' })
  public descricao: string

  @column({ columnName: 'cAtividade' })
  public ativo: string
}
