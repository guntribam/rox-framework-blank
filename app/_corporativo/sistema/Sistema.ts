import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'
import Env from '@ioc:Adonis/Core/Env'

export default class Sistema extends BaseModel {
  static table = `${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.SISTEMA`

  @column({ isPrimary: true, columnName: 'iCodigo' })
  public id: number

  @column({ columnName: 'sNome' })
  public nome: string

  @column({ columnName: 'sDescricao' })
  public descricao: string
}
