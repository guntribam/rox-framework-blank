import { BaseModel, BelongsTo, belongsTo, column } from '@ioc:Adonis/Lucid/Orm'
import Orgao from '../orgao/Orgao'
import Pessoa from '../pessoa/Pessoa'
import Sistema from '../sistema/Sistema'
import Env from '@ioc:Adonis/Core/Env'

export default class AcessoOrgao extends BaseModel {
  static table = `${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ORGAO`

  @column({ isPrimary: true, columnName: 'idAcessoOrgao' })
  id: number

  @column({ columnName: 'idSistema' })
  idSistema: number

  @column({ columnName: 'idPessoa' })
  idPessoa: number

  @column({ columnName: 'idOrgao' })
  idOrgao: number

  @column({ columnName: 'idTipoSigilo' })
  idTipoSigilo: number

  @belongsTo(() => Sistema, {
    foreignKey: 'idSistema',
    localKey: 'id',
  })
  sistema: BelongsTo<typeof Sistema>

  @belongsTo(() => Pessoa, {
    foreignKey: 'idPessoa',
    localKey: 'id',
  })
  pessoa: BelongsTo<typeof Pessoa>

  @belongsTo(() => Orgao, {
    foreignKey: 'idOrgao',
    localKey: 'id',
  })
  orgao: BelongsTo<typeof Orgao>
}
