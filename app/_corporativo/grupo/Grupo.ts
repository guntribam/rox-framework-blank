import { column, BaseModel, manyToMany, ManyToMany } from '@ioc:Adonis/Lucid/Orm'
import Pessoa from '../pessoa/Pessoa'
import Sistema from '../sistema/Sistema'
import Env from '@ioc:Adonis/Core/Env'

export default class Grupo extends BaseModel {
  static table = `${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.GRUPO`

  @column({ isPrimary: true, columnName: 'iCodigo' })
  id: number

  @column({ columnName: 'sDescricao' })
  descricao: string

  @manyToMany(() => Pessoa, {
    localKey: 'id',
    pivotForeignKey: 'iGrupo',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'iUsuario',
    pivotTable: `${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.R_GRUPO_USUARIO`,
  })
  usuarios: ManyToMany<typeof Pessoa>

  @manyToMany(() => Sistema, {
    localKey: 'id',
    pivotForeignKey: 'iGrupo',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'iSistema',
    pivotTable: `${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.R_GRUPO_SISTEMA`,
    pivotColumns: ['sDatabase', 'sServidor', 'cAmbiente']
  })
  sistemas: ManyToMany<typeof Sistema>
}
