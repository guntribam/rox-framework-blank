import { BaseModel, BelongsTo, belongsTo, column } from '@ioc:Adonis/Lucid/Orm'
import AcessoOrgao from '../acessoorgao/AcessoOrgao'
import PapelAcesso from '../papelacesso/PapelAcesso'
import Sistema from '../sistema/Sistema'
import Env from '@ioc:Adonis/Core/Env'

export default class PapelPessoaOrgao extends BaseModel {
  static table = `${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_PESSOA_ORGAO`

  @column({ isPrimary: true, columnName: 'idPapelPessoaOrgao' })
  id: number

  @column({ columnName: 'idSistema' })
  idSistema: number

  @column({ columnName: 'idPapelAcesso' })
  idPapelAcesso: number

  @column({ columnName: 'idAcessoOrgao' })
  idAcessoOrgao: number

  @belongsTo(() => Sistema, {
    foreignKey: 'idSistema',
    localKey: 'id',
  })
  sistema: BelongsTo<typeof Sistema>

  @belongsTo(() => PapelAcesso, {
    foreignKey: 'idPapelAcesso',
    localKey: 'id',
  })
  papelAcesso: BelongsTo<typeof PapelAcesso>

  @belongsTo(() => AcessoOrgao, {
    foreignKey: 'idAcessoOrgao',
    localKey: 'id',
  })
  acessoOrgao: BelongsTo<typeof AcessoOrgao>

}
