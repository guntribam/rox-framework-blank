import { BaseModel, BelongsTo, belongsTo, column } from '@ioc:Adonis/Lucid/Orm'
import { SimNaoConverter } from 'App/_infra/Converters'
import Sistema from '../sistema/Sistema'
import Env from '@ioc:Adonis/Core/Env'
export default class Acao extends BaseModel {
  static table = `${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACAO`

  @column({ isPrimary: true, columnName: 'idAcao' })
  id: number

  @column({ columnName: 'nmAcao' })
  nome: string

  @column({ columnName: 'idSistema' })
  idSistema: number

  @column({ columnName: 'stExigirDelegacao', ...SimNaoConverter })
  exigirDelegacao: boolean

  @column({ columnName: 'stRequerAcesso', ...SimNaoConverter })
  requerAcesso: boolean

  @column({ columnName: 'stAtivo', ...SimNaoConverter })
  ativo: boolean

  @belongsTo(() => Sistema, {
    foreignKey: 'idSistema',
    localKey: 'id',
  })
  sistema: BelongsTo<typeof Sistema>
}
