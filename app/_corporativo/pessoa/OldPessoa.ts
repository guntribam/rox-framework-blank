import { column, BaseModel, manyToMany, ManyToMany, hasOne, HasOne, belongsTo, BelongsTo } from '@ioc:Adonis/Lucid/Orm'
import { DateTime } from 'luxon'
import Grupo from '../grupo/Grupo'
import Orgao from '../orgao/Orgao'
import TipoPessoa from '../tipopessoa/TipoPessoa'
import UsuarioRede from '../usuariorede/UsuarioRede'

export default class Pessoa extends BaseModel {
  static table = 'MPDFT2.dbo.PESSOA'

  @column({ isPrimary: true, columnName: 'iCodigo' })
  id: number

  @column({ columnName: 'sNome' })
  nome: string

  @column({ columnName: 'iMatricula' })
  matricula: number

  @column({ columnName: 'iIdRede'})
  idUsuarioRede: number

  @column({ columnName: 'iTipo' })
  idTipoPessoa: number

  @column.date({ columnName: 'dtModificacao' })
  dataModificacao: DateTime

  @column({ columnName: 'iFiltro' })
  filtro: number

  @column({ columnName: 'msrepl_tran_version' })
  msreplTranVersion: string

  @belongsTo(() => UsuarioRede, { foreignKey: 'idUsuarioRede' })
  usuario: BelongsTo<typeof UsuarioRede>

  @hasOne(() => TipoPessoa, {
    foreignKey: "id",
    localKey: "idTipoPessoa"
  })
  tipo: HasOne<typeof TipoPessoa>

  @manyToMany(() => Grupo, {
    localKey: 'id',
    pivotForeignKey: 'iUsuario',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'iGrupo',
    pivotTable: 'MPDFT2.dbo.R_GRUPO_USUARIO',
  })
  gruposAcesso: ManyToMany<typeof Grupo>

  @manyToMany(() => Orgao, {
    localKey: 'id',
    pivotForeignKey: 'iPessoa',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'iOrgao',
    pivotTable: 'MPDFT2.dbo.R_LOTACAO',
    pivotColumns: ['dtInicio', 'dtFim']
  })
  lotacoes: ManyToMany<typeof Orgao>

}

