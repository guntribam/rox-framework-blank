import { column, BaseModel, manyToMany, ManyToMany, hasOne, HasOne, belongsTo, BelongsTo } from '@ioc:Adonis/Lucid/Orm'
import { DateTime } from 'luxon'
import Orgao from '../orgao/Orgao'
import TipoPessoa from '../tipopessoa/TipoPessoa'
import UsuarioRede from '../usuariorede/UsuarioRede'
import Env from '@ioc:Adonis/Core/Env'

export default class Pessoa extends BaseModel {
  static table = `${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PESSOA`


  @column({ isPrimary: true, columnName: 'iCodigo' })
  id: number

  @column({ columnName: 'sNome' })
  nome: string

  @column({ columnName: 'iMatricula' })
  matricula: number

  @column({ columnName: 'iIdRede'})
  idUsuarioRede: number

  @column({ columnName: 'iTipo' })
  idTipoPessoa: number

  @column.date({ columnName: 'dtModificacao' })
  dataModificacao: DateTime

  @column({ columnName: 'iFiltro' })
  filtro: number

  @column({ columnName: 'msrepl_tran_version' })
  msreplTranVersion: string

  @belongsTo(() => UsuarioRede, { foreignKey: 'idUsuarioRede' })
  usuario: BelongsTo<typeof UsuarioRede>

  @hasOne(() => TipoPessoa, {
    foreignKey: "id",
    localKey: "idTipoPessoa"
  })
  tipo: HasOne<typeof TipoPessoa>

  @manyToMany(() => Orgao, {
    localKey: 'id',
    pivotForeignKey: 'idPessoa',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'idOrgao',
    pivotTable: `${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ORGAO`,
  })
  orgaos: ManyToMany<typeof Orgao>

}

