import { BasePolicy as BaseUserPolicy } from '@ioc:Adonis/Addons/Bouncer'
import type { UserInfo } from 'Types'

export default abstract class BasePolicy extends BaseUserPolicy {
  protected actionSuffix: string;
	async view() { return true}
	async create(user: UserInfo) {
    return this.actionSuffix ? user.acessoAtual.acoes.some(a => a.nome === `CADASTRAR_${this.actionSuffix}`) : true;
  }
	async update(user: UserInfo) {
    return this.actionSuffix ? user.acessoAtual.acoes.some(a => a.nome === `ALTERAR_${this.actionSuffix}`) : true
  }
	async delete(user: UserInfo) {
    return this.actionSuffix ? user.acessoAtual.acoes.some(a => a.nome === `EXCLUIR_${this.actionSuffix}`): true
  }
}
