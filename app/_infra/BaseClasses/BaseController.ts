import type { RequestContract } from '@ioc:Adonis/Core/Request'

export interface FilterParams {
  page: number
  limit: number
  filter: any
  load: any
  sort: Array<{ column: string; order: 'asc' | 'desc' }>
}

export default class BaseController {
  getFilterParams(request: RequestContract, roleBasedFilter: any): FilterParams{
    return {
      page: request.input('page', 1),
      limit: request.input('limit', 10),
      filter: {...JSON.parse(request.input('filter', '{}')), ...roleBasedFilter},
      load: JSON.parse(request.input('load', '[]')),
      sort: JSON.parse(request.input('sort', '[{"column":"id","order":"asc"}]')),
    }
  }
}
