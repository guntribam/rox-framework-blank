import type { LucidModel } from '@ioc:Adonis/Lucid/Orm'
import * as enums from '../Enums'

type Sort = Array<{ column: string; order: 'asc' | 'desc' }>

interface BuildQueryParams<T extends LucidModel> {
  entity: T
  filter: any
  load: any
  sort: Sort
}

export default class BaseService {

  buildQuery<T extends LucidModel>({ entity, filter, load, sort }: BuildQueryParams<T>) {
    const sql =
      this.preloadEntity(load) +
      Object.keys(filter)
        .map((key) => `${this.renderSql(filter, key, entity, load)}`)
        .filter((k) => k)
        .join('')
    const orderBy = this.orderByBuilder(sort, entity)
    const query = `entity.query()${sql}${orderBy}`
    console.log({query})
    return eval(query)
  }

  preloadEntity(load) {
    return load
      .map((entity) => {
        const selectors = entity.split('.')
        const modifier = new Array(selectors.length).fill(')').join('')
        return selectors.map((e) => `.preload('${e}', ${e} => ${e}`).join('') + modifier
      })
      .join('')
  }

  orderByBuilder<T extends LucidModel>(sort: Sort, entity: T) {
    const orderBys = sort.map((s) => {
      if (s.column.includes('.')) {
        let subEntities: any = []
        const selectors = s.column.split('.')
        const field = selectors.pop()
        let query = selectors.reduce((acc, subEntity, index) => {
          const subQuery = `${acc}.$relationsDefinitions.get('${subEntity}')`
          if (index === selectors.length - 1) {
            const owner = subEntities.reduce((acc, el) => {
              return acc.$relationsDefinitions.get(`${el}`).relatedModel()
            }, entity)
            const subId = 'id' + selectors[index][0].toUpperCase() + selectors[index].slice(1)
            const ownerSubId = owner.$columnsDefinitions.get(`${subId}`)?.columnName
            const sortField = field === 'label' ? 'id' : field
            return `${subQuery}?.relatedModel().query().select('${sortField}').whereColumn('id','${owner.table}.${ownerSubId}')`
          }
          subEntities.push(subEntity)
          return subQuery
        }, `entity`)
        return `.orderBy(${query}, '${s.order}')`
      }
      return `.orderBy('${s.column}', '${s.order}')`
    })
    return `${orderBys.map((o) => o).join('')}`
  }

  renderSql(filter, key, entity, load) {
    if (key === 'query') return this.queryAll(filter[key], entity, load)
    if (filter[key] === null) return this.keyIsNull(key)
    if (filter[key]?.type === 'interval') return this.intervalBuilder(filter, key)
    if (filter[key]?.type === 'range') return this.rangeBuilder(filter, key)
    if (filter[key]?.type === 'checkbox') return this.checkboxBuilder(filter, key)
    if (key.includes('.')) return this.nestedProperty(key, filter)
    if (!entity.$getColumn(key)) return this.propertyNotFound(key)
    return this.equalTo(key, filter[key])
  }

  keyIsNull(key) {
    return `.whereNull('${key}')`
  }

  initial(key, value) {
    return `.where('${key}','>=', '${value}')`
  }

  end(key, value) {
    return `.where('${key}','<=', '${value}')`
  }

  equalTo(key, value) {
    return `.where('${key}','=','${value}')`
  }

  orEqualTo(key, value) {
    return `.orWhere('${key}','=','${value}')`
  }

  like(key, value) {
    return `.where('${key}','like','%${value}%')`
  }

  propertyNotFound(key) {
    console.log(`Filter property ignored: ${key}`)
  }

  nestedProperty(key, filter) {
    const selectors = key.split('.')
    const field = selectors.pop()
    //const condition = `.where('${field}','${filter[key]}')`
    //let sql = preloadEntity([selectors.join('.')])
    const filler = new Array(selectors.length).fill(')').join('')
    const modifier = `.where('${field}','${filter[key]}')${filler}`
    return selectors.map((e) => `.whereHas('${e}',${e}=>${e}`).join('') + modifier
  }

  rangeBuilder(filter, key) {
    let query = ''
    if (filter[key]?.from) query += this.initial(key, filter[key].from)
    if (filter[key]?.to) query += this.end(key, filter[key].to)
    return query
  }

  intervalBuilder(filter, key) {
    const dateMatcher = /^\d{4}-\d{2}-\d{2}$/
    let query = ''
    if (filter[key]?.from && filter[key].from.match(dateMatcher))
      query += this.initial(key, filter[key].from)
    if (filter[key]?.to && filter[key].to.match(dateMatcher)) query += this.end(key, filter[key].to)
    return query
  }

  checkboxBuilder(filter, key) {
    const values = filter[key].values
    const orEqualTo = this.orEqualTo
    return values > 0 ? values.map((v) => orEqualTo(key, v)).join('') : ''
  }

  queryAll(value, entity, load) {
    const queryProps = value.split(/(\sE\s|\sOU\s)/g).map((e) => e.replace(/\s+/g, ' ').trim())
    let filterCode = 'E'
    return queryProps
      .map((p) => {
        if (p === 'E' || p === 'OU') {
          filterCode = p
          return null
        } else {
          return this.queryAllByValue(p, entity, load, filterCode === 'E' ? 'andWhere' : 'orWhere')
        }
      })
      .filter((p) => p)
      .join('')
  }

  queryAllByValue(value: string, entity, load, where) {
    value = value.match(/\d{2}\/(\d{2}\/\d{4}|\d{2})/g)
      ? value.split('/').reverse().join('-')
      : value
    let query = Array.from(entity.$columnsDefinitions.keys())
      .filter((key: string) => !key.includes('id'))
      .reduce(
        (query, column) => `${query}.orWhere('${column}', 'like', '%${value}%')`,
        `.${where}(query => query`
      )
    return (
      load.reduce((query, loadKey: string) => {
        const relatedModel = loadKey.split('.').reduce((deepEntity, relation) => {
          return deepEntity.$relationsDefinitions.get(relation).relatedModel()
        }, entity)
        const keys = relatedModel.$columnsDefinitions.keys()
        const whereLike = Array.from(keys)
          .filter((key: string) => !key.includes('id'))
          .map((columnKey) => {
            const myEnum = enums[`${loadKey[0].toUpperCase()}${loadKey.slice(1)}Enum`]
            if (myEnum) {
              const entry: any = Object.entries(myEnum).find((e) =>
                e[0].toLowerCase().includes(value.toLowerCase())
              )
              if (entry) return `.andWhere('${columnKey}', '=', '${entry[1].tipo}')`
            }
            return `.orWhere('${columnKey}', 'like', '%${value}%')`
          })
          .join('')

        const whereHas = loadKey.split('.').reduce((acc, key) => {
          return `${acc}.orWhereHas('${key}', ${key} => ${key}`
        }, '')
        const filler = new Array(loadKey.split('.').length).fill(')').join('')
        return `${query}${whereHas}${whereLike}${filler}`
      }, query) + ')'
    )
  }
}
