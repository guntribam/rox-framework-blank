import type { Papeis, UserInfo } from 'Types'

type Rules = {
  [key in keyof typeof Papeis]?: (user: UserInfo) => any
}

export default class BaseRoleFilter {
  roleBasedFilter: Rules = {
    chefe: (_: UserInfo) => ({}),
    funcionario: (_: UserInfo) => ({}),
    estagiario: (_: UserInfo) => ({}),
  }

  filter(user: UserInfo) {
    const filter = this.roleBasedFilter[user?.acessoAtual?.papelAtual?.nome]
    return filter ? filter(user) : {}
  }
}
