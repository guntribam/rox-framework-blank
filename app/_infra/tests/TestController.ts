import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import { appBaseData } from 'Database/samples/AppSamples'
import { corporativoBaseData } from 'Database/samples/corporativo/CorporativoSamples'
import { cleanTableData } from 'Database/samples/DeleteSamples'
import { restoreCacheBaseState, resetCache, initCacheBaseState } from 'Database/samples/SamplesUtils'

export default class TestController {
  async seed({request}: HttpContextContract) {
    const {seederName} = request.all()
    const seeder = (await import(`../../../database/seeders/testdata/${seederName}.ts`)).default
    const dictionary = await seeder.seed();
    return dictionary;
  }

  async initTestBaseState() {
    await cleanTableData();
    await corporativoBaseData();
    await appBaseData();
    initCacheBaseState();
    await Database.beginGlobalTransaction();
  }

  async restoreTestBaseState() {
    await Database.rollbackGlobalTransaction()
    restoreCacheBaseState();
    await Database.beginGlobalTransaction()
  }

  async resetTestBaseState() {
    await Database.commitGlobalTransaction()
    resetCache();
  }

}
