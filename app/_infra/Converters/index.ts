export const SimNaoConverter = {
  prepare: (value: boolean) => value ? 'S' : 'N',
  consume: (value: 'S' | 'N') => value === 'S',
}
