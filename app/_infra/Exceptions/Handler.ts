/*
|--------------------------------------------------------------------------
| Http Exception Handler
|--------------------------------------------------------------------------
|
| AdonisJs will forward all exceptions occurred during an HTTP request to
| the following class. You can learn more about exception handling by
| reading docs.
|
*/
import { AuthorizationException } from '@adonisjs/bouncer/build/src/Exceptions/AuthorizationException'
import ValidacaoException from './ValidacaoException'

const exceptionTypes = [
  { exception: AuthorizationException, message: (_: any) => 'Usuário sem permissão de acesso' },
  { exception: ValidacaoException, message: (message: string) => message },
]
export default class ExceptionHandler {
  readonly ignoreStatus: number[]

  constructor() {
    this.ignoreStatus = []
  }

  context(ctx) {
    const requestId = ctx.request.id()
    return requestId ? { 'x-request-id': requestId } : {}
  }

  shouldReport(error) {
    if (error.status && this.ignoreStatus.indexOf(error.status) > -1) {
      return false
    }
    return true
  }

  report(error, ctx) {
    error.status = error.status || 500
    if (!this.shouldReport(error)) {
      return
    }
    if (typeof error.report === 'function') {
      error.report(error, ctx)
      return
    }
    /**
     * - Log level = `error` para `500 e superior`
     * - Log level = `warn` para `400 e superior`
     * - Log level = `info` para o resto.
     */
    const loggerFn = error.status >= 500 ? 'error' : error.status >= 400 ? 'warn' : 'info'
    ctx.logger[loggerFn](error)
  }

  async makeJSONResponse(error, ctx) {
    const exceptionType = exceptionTypes.find(({ exception }) => error instanceof exception)
    const defaultMsg = 'Erro no sistema, contate a STI.'
    //error.message = exceptionType?.message ?? "Erro no sistema. Entre em contato com a STI"
    if (process.env.NODE_ENV === 'development') {
      ctx.response.status(error.status).send({
        errors: [
          {
            message: exceptionType?.message(error.message) ?? defaultMsg,
            stack: error.stack,
            code: error.code,
          },
        ],
      })
      return
    }
    ctx.response
      .status(error.status)
      .send({ errors: [{ message: exceptionType?.message(error.message) ?? defaultMsg }] })
  }

  async handle(error, ctx) {
    error.status = error.status || 500
    if (typeof error.handle === 'function') {
      return error.handle(error, ctx)
    }

    return this.makeJSONResponse(error, ctx)
  }
}
