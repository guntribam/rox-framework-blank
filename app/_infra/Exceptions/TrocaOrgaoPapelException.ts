import { Exception } from '@adonisjs/core/build/standalone'

/*
|--------------------------------------------------------------------------
| Exception
|--------------------------------------------------------------------------
|
| The Exception class imported from `@adonisjs/core` allows defining
| a status code and error code for every exception.
|
| @example
| new TrocaOrgaoPerfilException('message', 500, 'E_RUNTIME_EXCEPTION')
|
*/
export default class TrocaOrgaoPapelException extends Exception {
  constructor(public message: string) {
    super(message, 400)
  }

  static orgao() {
    return new this("Erro ao trocar órgão")
  }
  static perfil() {
    return new this("Erro ao trocar perfil")
  }
}
