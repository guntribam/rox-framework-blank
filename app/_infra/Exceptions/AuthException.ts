import { Exception } from '@adonisjs/core/build/standalone'
export class AuthException extends Exception {
  constructor(public message: string, public isLogout = false) {
    super(message, 401)
  }
  static ldapError(ldapError: Error) {
    return new this(`Erro ao autenticar no LDAP: ${ldapError.message}`)
  }
  static usuarioSenhaNaoCodificadoBase64() {
    return new this('Usuário/senha não codificados em Base64')
  }
  static formatoHeaderNaoReconhecido() {
    return new this('Usuário/senha informados em formato distinto ao Basic Authorization')
  }
  static headerNaoInformado() {
    return new this('Header "Authorization" não informado')
  }
  static usuarioSenhaNaoAderenteBASIC() {
    return new this('Usuário/senha decodificados em formato diferente à <username>:<password>')
  }
  static usuarioSenhaNaoInformados() {
    return new this('Usuário/senha não informado(s)')
  }
  static usuarioNaoEncontrado() {
    return new this('Usuário não encontrado para as credenciais informadas')
  }
  static senhaIncorreta() {
    return new this('Senha incorreta para usuário informado')
  }
  static logout() {
    return new this('Logout', true)
  }

  async handle(_, ctx) {
    if (ctx.request.ajax()) {
      ctx.response.status(this.status).send({
        errors: [{ message: this.message }],
      })
    } else {
      const config = ctx.auth.use('basicCustom').config

      // const objectToSend = {
      //   message: this.message,
      //   stack: this.stack,
      //   code: this.code,
      // }
      ctx.response
        .status(this.status)
        .header(
          this.isLogout ? '' : 'WWW-Authenticate',
          this.isLogout ? '' : `Basic realm="${config.realm}", charset="UTF-8"`
        )
        .send({errors: [{message: "Acesso negado"}]})
    }
  }
}
