import type { LucidModel } from '@ioc:Adonis/Lucid/Orm'
import * as enums from '../Enums'

type Sort = Array<{ column: string; order: 'asc' | 'desc' }>


interface BuildQueryParams<T extends LucidModel> {
  entity: T
  filter: any
  load: any
  sort: Sort
}


export function buildQuery<T extends LucidModel>({
  entity,
  filter,
  load,
  sort,
}: BuildQueryParams<T>) {
  const sql =
    preloadEntity(load) +
    Object.keys(filter)
      .map((key) => `${renderSql(filter, key, entity, load)}`)
      .filter((k) => k)
      .join('')
  const orderBy = orderByBuilder(sort, entity)
  return eval(`entity.query()${sql}${orderBy}`)
}

function orderByBuilder<T extends LucidModel>(sort: Sort, entity: T) {
  const orderBys = sort.map((s) => {
    if (s.column.includes('.')) {
      let subEntities: any = []
      const selectors = s.column.split('.')
      const field = selectors.pop()
      let query = selectors.reduce((acc, subEntity, index) => {
        const subQuery = `${acc}.$relationsDefinitions.get('${subEntity}')`
        if (index === selectors.length - 1) {
          const owner = subEntities.reduce((acc, el) => {
            return acc.$relationsDefinitions.get(`${el}`).relatedModel()
          }, entity)
          const subId = 'id' + selectors[index][0].toUpperCase() + selectors[index].slice(1)
          const ownerSubId = owner.$columnsDefinitions.get(`${subId}`)?.columnName
          const sortField = field === 'label' ? 'id' : field
          return `${subQuery}?.relatedModel().query().select('${sortField}').whereColumn('id','${owner.table}.${ownerSubId}')`
        }
        subEntities.push(subEntity)
        return subQuery
      }, `entity`)
      return `.orderBy(${query}, '${s.order}')`
    }
    return `.orderBy('${s.column}', '${s.order}')`
  })
  return `${orderBys.map((o) => o).join('')}`
}

const preloadEntity = (load) => {
  return load.map((entity) => {
      const selectors = entity.split('.')
      const modifier = new Array(selectors.length).fill(')').join('')
      return selectors.map((e) => `.preload('${e}', ${e} => ${e}`).join('') + modifier
    }).join('')
}

const renderSql = (filter, key, entity, load) => {
  if (key === 'query') return filterRenderer.queryAll(filter[key], entity, load)
  if (filter[key] === null) return filterRenderer.keyIsNull(key)
  if (filter[key]?.type === 'interval') return filterRenderer.intervalBuilder(filter, key)
  if (filter[key]?.type === 'range') return filterRenderer.rangeBuilder(filter, key)
  if (filter[key]?.type === 'checkbox') return filterRenderer.checkboxBuilder(filter, key)
  if (key.includes('.')) return filterRenderer.nestedProperty(key, filter)
  if (!entity.$getColumn(key)) return filterRenderer.propertyNotFound(key)
  return filterRenderer.equalTo(key, filter[key])
}

const filterRenderer = {
  keyIsNull: (key) => `.whereNull('${key}')`,
  initial: (key, value) => `.where('${key}','>=', '${value}')`,
  end: (key, value) => `.where('${key}','<=', '${value}')`,
  equalTo: (key, value) => `.where('${key}','=','${value}')`,
  orEqualTo: (key, value) => `.orWhere('${key}','=','${value}')`,
  like: (key, value) => `.where('${key}','like','%${value}%')`,
  propertyNotFound: (key) => console.log(`Filter property ignored: ${key}`),
  nestedProperty: (key, filter) => {
    const selectors = key.split('.')
    const field = selectors.pop()
    //const condition = `.where('${field}','${filter[key]}')`
    //let sql = preloadEntity([selectors.join('.')])
    const filler = new Array(selectors.length).fill(')').join('')
    const modifier = `.where('${field}','${filter[key]}')${filler}`
    return selectors.map((e) => `.whereHas('${e}',${e}=>${e}`).join('') + modifier
  },
  rangeBuilder: function (filter, key) {
    let query = ''
    if (filter[key]?.from) query += this.initial(key, filter[key].from)
    if (filter[key]?.to) query += this.end(key, filter[key].to)
    return query
  },
  intervalBuilder: function (filter, key) {
    const dateMatcher = /^\d{4}-\d{2}-\d{2}$/
    let query = ''
    if (filter[key]?.from && filter[key].from.match(dateMatcher))
      query += this.initial(key, filter[key].from)
    if (filter[key]?.to && filter[key].to.match(dateMatcher)) query += this.end(key, filter[key].to)
    return query
  },
  checkboxBuilder: function (filter, key) {
    const values = filter[key].values
    const orEqualTo = this.orEqualTo
    return values > 0 ? values.map((v) => orEqualTo(key, v)).join('') : ''
  },
  queryAll: function (value, entity, load) {
    const queryProps = value.split(/(\sE\s|\sOU\s)/g).map((e) => e.replace(/\s+/g, ' ').trim())
    let filterCode = 'E'
    return queryProps
      .map((p) => {
        if (p === 'E' || p === 'OU') {
          filterCode = p
          return null
        } else {
          return queryAllByValue(p, entity, load, filterCode === 'E' ? 'andWhere' : 'orWhere')
        }
      })
      .filter((p) => p)
      .join('')
  },
}

function queryAllByValue(value: string, entity, load, where) {
  value = value.match(/\d{2}\/(\d{2}\/\d{4}|\d{2})/g) ? value.split('/').reverse().join('-') : value
  let query = Array.from(entity.$columnsDefinitions.keys())
    .filter((key: string) => !key.includes('id'))
    .reduce(
      (query, column) => `${query}.orWhere('${column}', 'like', '%${value}%')`,
      `.${where}(query => query`
    )
  return (
    load.reduce((query, loadKey: string) => {
      const relatedModel = loadKey.split('.').reduce((deepEntity, relation) => {
        return deepEntity.$relationsDefinitions
          .get(relation)
          .relatedModel()
      },entity)
      const keys = relatedModel.$columnsDefinitions.keys()
      const whereLike = Array.from(keys)
        .filter((key: string) => !key.includes('id'))
        .map((columnKey) => {
          const myEnum = enums[`${loadKey[0].toUpperCase()}${loadKey.slice(1)}Enum`]
          if (myEnum) {
            const entry: any = Object.entries(myEnum).find((e) =>
              e[0].toLowerCase().includes(value.toLowerCase())
            )
            if (entry) return `.andWhere('${columnKey}', '=', '${entry[1].tipo}')`
          }
          return `.orWhere('${columnKey}', 'like', '%${value}%')`
        })
        .join('')

        const whereHas = loadKey.split('.').reduce((acc, key) => {
          return `${acc}.orWhereHas('${key}', ${key} => ${key}`
        },'')
        const filler = new Array(loadKey.split('.').length).fill(')').join('')
      return `${query}${whereHas}${whereLike}${filler}`
    }, query) + ')'
  )
}
