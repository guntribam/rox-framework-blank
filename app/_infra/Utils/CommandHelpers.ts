import fs from 'fs'

export async function createFile(location: string, stubLocation: string, valuesToTemplate: any, logger: any, generator: any){
  if (fs.existsSync(location)) {
    logger.skip(`${location} already exists`)
  } else {
    await generator.addFile(location).stub(stubLocation).useMustache().apply(valuesToTemplate)
    await generator.run()
    await generator.clear();
    logger.success(location)
  }
}

export function getDatabaseModelGeneratorConfig(){
  //config()
  return {
    client: 'mssql',
    connection: {
      host: process.env.MSSQL_SERVER as string,
      server: process.env.MSSQL_SERVER as string,
      port: process.env.MSSQL_PORT as string,
      user: process.env.MSSQL_USER as string,
      password: process.env.MSSQL_PASSWORD as string,
      database: process.env.MSSQL_DB_NAME as string,
    },
    interfaceNameFormat: "${table}",
    excludedTables: ["dbo.adonis_schema"]
  }
}
