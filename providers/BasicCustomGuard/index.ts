import { HttpContext } from '@adonisjs/http-server/build/standalone'
import type { GuardContract, ProvidersList } from '@ioc:Adonis/Addons/Auth'
import { base64 } from '@ioc:Adonis/Core/Helpers'
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import type { UserInfo } from 'Types'
import { AuthException } from 'App/_infra/Exceptions/AuthException'
import type { LdapProvider } from '../LdapProvider'

/**
 * RegExp for basic auth credentials.
 * Copy/pasted from https://github.com/jshttp/basic-auth/blob/master/index.js
 *
 * credentials = auth-scheme 1*SP token68
 * auth-scheme = "Basic" ; case insensitive
 * token68     = 1*( ALPHA / DIGIT / "-" / "." / "_" / "~" / "+" / "/" ) *"="
 */
const CREDENTIALS_REGEXP = /^ *(?:[Bb][Aa][Ss][Ii][Cc]) +([A-Za-z0-9._~+/-]+=*) *$/
/**
 * RegExp for basic auth user/pass
 * Copy/pasted from https://github.com/jshttp/basic-auth/blob/master/index.js
 *
 * user-pass   = userid ":" password
 * userid      = *<TEXT excluding ":">
 * password    = *TEXT
 */
const USER_PASS_REGEXP = /^([^:]*):(.*)$/

export type BasicCustomGuardConfig<Provider extends keyof ProvidersList> = {
  driver: 'basicCustom'
  realm?: string
  provider: ProvidersList[Provider]['config']
}

export class BasicCustomGuard implements GuardContract<'user', 'basicCustom'> {
  constructor(
    public name: 'basicCustom',
    public config: BasicCustomGuardConfig<'user'>,
    public provider: LdapProvider,
    public ctx: HttpContextContract,
    public isLoggedOut: boolean = false,
    public isAuthenticated: boolean = false,
    public authenticationAttempted: boolean = false,
    public user?: UserInfo | undefined
  ) {}

  get driver() {
    return this.config.driver
  }
  get isLoggedIn() {
    return !!this.user
  }
  get isGuest() {
    return !this.isLoggedIn
  }

  async verifyPassword(userProvider, password) {
    const verified = await userProvider.verifyPassword(password)
    if (!verified) {
      throw AuthException.senhaIncorreta()
    }
  }

  async lookupUsingUid(uid) {
    const userProvider = await this.provider.findByUsername(uid)
    if (!userProvider.user) {
      throw AuthException.usuarioNaoEncontrado()
    }
    return userProvider
  }
  async verifyCredentials(uid: string, password: string): Promise<UserInfo> {
    if (!uid || !password) {
      throw AuthException.usuarioSenhaNaoInformados()
    }
    const { session } = HttpContext.get()!
    let user = session.get('user')
    if (!user) {
      const userProvider = await this.lookupUsingUid(uid)
      await this.verifyPassword(userProvider, password)
      session.put('user', userProvider.user!)
      return userProvider.user as UserInfo
    }
    return user as UserInfo
  }

  getCredentials() {
    const credentials = this.ctx.request.header('Authorization')
    if (!credentials) throw AuthException.headerNaoInformado()

    const match = CREDENTIALS_REGEXP.exec(credentials)
    if (!match) throw AuthException.formatoHeaderNaoReconhecido()

    const decoded = base64.decode(match[1], 'utf-8', true)
    if (!decoded) throw AuthException.usuarioSenhaNaoCodificadoBase64()

    const user = USER_PASS_REGEXP.exec(decoded)
    if (!user) throw AuthException.usuarioSenhaNaoAderenteBASIC()
    return { uid: user[1], password: user[2] }
  }

  async authenticate(): Promise<UserInfo> {
    const credentials = this.getCredentials()
    const user = await this.verifyCredentials(credentials.uid, credentials.password)
    this.user = user
    this.isLoggedOut = false
    this.isAuthenticated = true
    //this.emitter.emit('adonis:basic:authenticate', this.getAuthenticateEventData(user))
    return this.user as UserInfo
  }
  async check(): Promise<boolean> {
    await this.authenticate()
    return this.isAuthenticated
  }

  logout(...args: any[]): Promise<void> {
    throw new Error('Method not implemented.')
    args
  }
  toJSON() {
    throw new Error('Method not implemented.')
  }
  attempt(uid: string, password: string, ...args: any[]): Promise<any> {
    throw new Error('Method not implemented.')
    console.log(uid, password, args)
  }
  login(user: UserInfo, ...args: any[]): Promise<any> {
    throw new Error('Method not implemented.')
    console.log(user, args)
  }
  loginViaId(id: string | number, ...args: any[]): Promise<any> {
    throw new Error('Method not implemented.')
    console.log(id, args)
  }
}
