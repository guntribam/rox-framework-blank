import type { ProviderUserContract, UserProviderContract } from '@ioc:Adonis/Addons/Auth'
import Env from '@ioc:Adonis/Core/Env'
import Logger from '@ioc:Adonis/Core/Logger'
import Database from '@ioc:Adonis/Lucid/Database'
import type { AcessoUsuarioAplicacao, UserInfo } from 'Types'
import { Exception } from '@poppinss/utils'
import PapelAcesso from 'App/_corporativo/papelacesso/PapelAcesso'
import Sistema from 'App/_corporativo/sistema/Sistema'
import { AuthException } from 'App/_infra/Exceptions/AuthException'
import { Client, SearchResult } from 'ldapts'

export class LdapUser implements ProviderUserContract<UserInfo> {
  constructor(public user: UserInfo | null, private config: LdapProviderConfig) {}

  public getId() {
    return this.user ? this.user.id : null
  }

  public async verifyPassword(plainPassword: string): Promise<boolean> {
    return await this.isAuthenticated(plainPassword)
  }
  public getRememberMeToken() {
    throw new Exception('Não implementado -> getRememberMeToken')
    return null
  }

  public setRememberMeToken(token: string) {
    throw new Exception('Não implementado -> setRememberMeToken' + token)
  }

  private async isAuthenticated(password: string): Promise<boolean> {
    const { ldapHost: url } = this.config
    const client = new Client({ url })
    let isAuthenticated
    try {
      await client.bind(this.user?.userDN ?? '', password)
      isAuthenticated = true
    } catch (ex) {
      isAuthenticated = false
    } finally {
      await client.unbind()
    }
    return isAuthenticated
  }
}

export type LdapProviderConfig = {
  driver: 'ldap'
  connection: string
  ldapHost: string
  ldapAdminDn: string
  ldapAdminPassword: string
  ldapUserSearchBase: string
  ldapUserAttribute: string
}

export class LdapProvider implements UserProviderContract<UserInfo> {
  constructor(private config: LdapProviderConfig) {}

  public async getUserFor(user: UserInfo | null) {
    return new LdapUser(user, this.config)
  }

  public async findByUsername(login: string) {
    const userDN = await this.findUserDNByUsername(login)
    const sistema = await this.getSistema(Env.get('NOME_SISTEMA_BANCO_CORPORATIVO'))
    const pessoaComAcesso: Omit<UserInfo, 'userDN'> = await this.getPessoaComAcesso(
      login,
      sistema
    )
    return this.getUserFor({ ...pessoaComAcesso, userDN })
  }

  private async getSistema(nomeSistema: string) {
    return this.logError(
      Sistema.findByOrFail('nome', nomeSistema),
      'não foi possível consultar Sistema'
    )
  }

  private async logError<T>(query: Promise<T>, errorMessage: string): Promise<T> {
    try {
      return await query
    } catch (err) {
      Logger.error(`Erro na autenticação: ${errorMessage}`)
      throw new Error(err)
    }
  }

  private async getPessoaComAcesso(
    login: string,
    sistema: Sistema
  ): Promise<Omit<UserInfo, 'userDN'>> {
    const prefix = Env.get('MSSQL_CORPORATIVO_DB_NAME') + '.dbo.'
    const acessoBD = await this.logError(
      Database.rawQuery(
        `
        SELECT
            p.iCodigo as idPessoa,
            p.sNome as nome,
            ur.sLogin as login,
            ttp.iCodigo as idTipoPessoa,
            ttp.sDescricao as descricaoTipoPessoa,
            o.iCodigo as idOrgao,
            o.sSigla as siglaOrgao,
            o.sNome as nomeOrgao,
            o.sNomeExtendido as nomeOrgaoExtendido,
            pac.idPapelAcesso as idPapelAcesso,
            pac.nmPapelAcesso as nomePapelAcesso,
            a.idAcao as idAcao,
            a.nmAcao as nomeAcao
        FROM
            ${prefix}PESSOA p
            JOIN ${prefix}T_TIPO_PESSOA ttp on ttp.iCodigo=p.iTipo
            JOIN ${prefix}USUARIO_REDE ur on ur.iRID=p.iIdRede
            JOIN ${prefix}ACESSO_ORGAO ao on ao.idPessoa=p.iCodigo
            JOIN ${prefix}ORGAO o on ao.idOrgao=o.iCodigo
            JOIN ${prefix}PAPEL_PESSOA_ORGAO ppo on ppo.idAcessoOrgao=ao.idAcessoOrgao
            JOIN ${prefix}PAPEL_ACESSO pac on pac.idPapelAcesso=ppo.idPapelAcesso
            JOIN ${prefix}PAPEL_ACAO pa on pa.idPapelAcesso=pac.idPapelAcesso
            JOIN ${prefix}ACAO a on a.idAcao=pa.idAcao
            JOIN ${prefix}SISTEMA s on s.iCodigo = ao.idSistema and s.iCodigo=ppo.idSistema and pac.idSistema=s.iCodigo and pa.idSistema=s.iCodigo and a.idSistema=s.iCodigo
        WHERE
            s.iCodigo=:idSistema and ur.sLogin=:login
    `,
        { idSistema: sistema.id, login: login }
      ),
      'Não foi possível consultar a pessoa'
    )

    const papeisAplicacao = await PapelAcesso.all()
    const acesso = this.buildAcesso(acessoBD)
    return {
      id: acessoBD[0]?.idPessoa,
      nome: acessoBD[0]?.nome,
      login: acessoBD[0]?.login.trim(),
      tipo: { id: acessoBD[0]?.idTipoPessoa, descricao: acessoBD[0]?.descricaoTipoPessoa },
      acesso,
      acessoAtual: { ...acesso[0], papelAtual: acesso[0].papeis[0] },
      papeisAplicacao,
    }
  }

  private buildAcesso = (acessoBD: any): AcessoUsuarioAplicacao[] => {
    return acessoBD.reduce((acesso, novo) => {
      const novoOrgao = !acesso.find((a) => a.orgao.id === novo.idOrgao)
      if (novoOrgao) {
        acesso.push({
          orgao: {
            id: novo.idOrgao,
            sigla: novo.siglaOrgao.trim(),
            nome: novo.nomeOrgao,
            nomeExtendido: novo.nomeOrgaoExtendido,
          },
          papeis: [{ id: novo.idPapelAcesso, nome: novo.nomePapelAcesso }],
          acoes: [{ id: novo.idAcao, nome: novo.nomeAcao }],
        })
      } else {
        const novoPapel = !acesso[acesso.length - 1].papeis.find((p) => p.id === novo.idPapelAcesso)
        novoPapel &&
          acesso[acesso.length - 1].papeis.push({
            id: novo.idPapelAcesso,
            nome: novo.nomePapelAcesso,
          })

        const novaAcao = !acesso[acesso.length - 1].acoes.find((a) => a.id === novo.idAcao)
        novaAcao &&
          acesso[acesso.length - 1].acoes.push({
            id: novo.idAcao,
            nome: novo.nomeAcao,
          })
      }
      return acesso
    }, [])
  }

  private async findUserDNByUsername(username: string): Promise<string> {
    const client = new Client({ url: this.config.ldapHost })
    let searchResult: SearchResult
    try {
      await client.bind(this.config.ldapAdminDn, this.config.ldapAdminPassword)
      searchResult = await client.search(this.config.ldapUserSearchBase, {
        scope: 'sub',
        filter: `(uid=${username})`,
      })
    } catch (err) {
      throw AuthException.ldapError(err)
    } finally {
      await client.unbind()
    }
    if (searchResult.searchEntries.length === 0) {
      throw AuthException.usuarioNaoEncontrado()
    }
    return searchResult.searchEntries[0].dn
  }

  public async findByRememberMeToken(_: string | number, __: string) {
    Logger.error('Não implementado -> findByRememberMeToken', console.trace())
    throw new Error()
    return new LdapUser(null, this.config)
  }

  public async findByUid(_: string) {
    return this.getUserFor(null)
  }

  public async updateRememberMeToken() {
    Logger.error('Não implementado -> updateRememberMeToken')
    throw new Error()
  }

  public async findById(id: string | number) {
    return this.getUserFor(null) ?? id
  }
}
