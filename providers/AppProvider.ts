import type { ApplicationContract } from '@ioc:Adonis/Core/Application'

export default class AppProvider {
  constructor(protected app: ApplicationContract) {}

  public  register() {
    // Register your own bindings
  }

  public async boot() {
    // IoC container is ready

    //Default naming strategy for ORM
    const { BaseModel } = await import('@ioc:Adonis/Lucid/Orm')
    const { AppNamingStrategy } = await import('Database/naming/AppNamingStrategy')
    BaseModel.namingStrategy = new AppNamingStrategy()
    //Default provider for BASIC login
    const { LdapProvider } = await import('./LdapProvider')
    const auth = this.app.container.use('Adonis/Addons/Auth')
    auth.extend('provider', 'ldap', (_, __, config) => new LdapProvider(config))

    //Custom guard for BASIC login
    const { BasicCustomGuard } = await import('./BasicCustomGuard')
    auth.extend('guard', 'basicCustom', (_, __, config, provider: any, ctx) =>
      new BasicCustomGuard('basicCustom', config, provider, ctx)
    )
    //Default timezone to prevent js Date errors
    const { Settings } = await import('luxon')
    //@ts-ignore
    Settings.defaultZoneName = 'utc'


  }

  public async ready() {
  }

  public async shutdown() {
    // Cleanup, since app is going down
  }
}
