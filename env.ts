/*
|--------------------------------------------------------------------------
| Validating Environment Variables
|--------------------------------------------------------------------------
|
| In this file we define the rules for validating environment variables.
| By performing validation we ensure that your application is running in
| a stable environment with correct configuration values.
|
| This file is read automatically by the framework during the boot lifecycle
| and hence do not rename or move this file to a different location.
|
*/

import Env from '@ioc:Adonis/Core/Env'

export default Env.rules({
  HOST: Env.schema.string({ format: 'host' }),
  PORT: Env.schema.number(),
  APP_KEY: Env.schema.string(),
  APP_NAME: Env.schema.string(),
  LOG_LEVEL: Env.schema.enum(['error', 'info'] as const),
  NODE_ENV: Env.schema.enum(['localhost', 'development', 'staging', 'production'] as const),
  NOME_SISTEMA_BANCO_CORPORATIVO: Env.schema.string(),
  MSSQL_SERVER: Env.schema.string(),
  MSSQL_PORT: Env.schema.number(),
  MSSQL_INSTANCE: Env.schema.string.optional(),
  MSSQL_DATABASE: Env.schema.string(),
  MSSQL_DOMAIN: Env.schema.string.optional(),
  MSSQL_APP_DB_NAME: Env.schema.string(),
  MSSQL_APP_DB_USER: Env.schema.string(),
  MSSQL_APP_DB_PASSWORD: Env.schema.string.optional(),
  MSSQL_APP_DB_PREFIX: Env.schema.string(),
  MSSQL_CORPORATIVO_DB_NAME: Env.schema.string.optional(),
  RUN_LOCALHOST_MIGRATIONS: Env.schema.boolean.optional(),
  LDAP_HOST: Env.schema.string(),
  LDAP_ADMIN_DN: Env.schema.string(),
  LDAP_ADMIN_PASSWORD: Env.schema.string(),
  LDAP_USER_SEARCH_BASE: Env.schema.string(),
  CORS_ALLOWED_ORIGINS: Env.schema.string(),
  SESSION_DRIVER: Env.schema.string()
})
