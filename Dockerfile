FROM node:16.14.0-alpine3.15 as installer
WORKDIR /app
COPY ./ ./
RUN npm i -g pnpm 
RUN pnpm install 
RUN pnpm build

FROM node:16.14.0-alpine3.15 as buildImage
WORKDIR /app
COPY --from=installer ./app/pnpm-lock.yaml ./
COPY --from=installer ./app/package.json ./
COPY --from=installer ./app/build/ .
RUN npm i -g pnpm 
RUN pnpm install
CMD node server | tee server.log


