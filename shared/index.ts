export type {default as Orgao} from "../app/_corporativo/orgao/Orgao";

export type {DateFormat} from '../database/factories/FactoryUtils'

export enum Papeis {
  chefe = 'Chefe',
  funcionario = 'Funcionário',
  estagiario = 'Estagiário'
}

export interface AcessoUsuarioAplicacao {
    orgao: any,
    papeis: {id: number, nome: string}[],
    acoes: {id: number, nome: string}[]
  }

export interface UserInfo {
    id: number,
    nome: string,
    login: string,
    tipo: {id: number, descricao: string}
    acesso: AcessoUsuarioAplicacao[],
    acessoAtual: AcessoUsuarioAplicacao & {papelAtual: {id: number, nome: string}},
    userDN: string,
    papeisAplicacao: any[]
}



