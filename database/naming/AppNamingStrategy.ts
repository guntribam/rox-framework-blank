import type { NamingStrategyContract, LucidModel } from '@ioc:Adonis/Lucid/Orm'
import { string } from '@ioc:Adonis/Core/Helpers'
import Env from '@ioc:Adonis/Core/Env'

export class AppNamingStrategy implements NamingStrategyContract {
  tableName(model: LucidModel) {
    const tableName = string.snakeCase(model.name).toUpperCase()
    return Env.get('MSSQL_APP_DB_PREFIX') + tableName
  }

  columnName(_model: LucidModel, propertyName: string) {
    return string.camelCase(propertyName)
  }

  serializedName(_model: LucidModel, propertyName: string) {
    return string.camelCase(propertyName)
  }

  relationLocalKey(relation: string, model: LucidModel, relatedModel: LucidModel) {
    if (relation === 'belongsTo') {
      return relatedModel.primaryKey
    }

    return model.primaryKey
  }

  relationForeignKey(relation: string, model: LucidModel, relatedModel: LucidModel) {
    if (relation === 'belongsTo') {
      return string.camelCase(`${relatedModel.primaryKey}_${relatedModel.name}`)
    }

    return string.camelCase(`${model.primaryKey}_${model.name}`)
  }

  relationPivotTable(_relation: 'manyToMany', model: LucidModel, relatedModel: LucidModel) {
    return string.snakeCase([relatedModel.name, model.name].sort().join('_'))
  }

  relationPivotForeignKey(_relation: 'manyToMany', model: LucidModel) {
    return string.snakeCase(`${model.primaryKey}_${model.name}`)
  }

  paginationMetaKeys() {
    return {
      total: 'total',
      perPage: 'perPage',
      currentPage: 'currentPage',
      lastPage: 'lastPage',
      firstPage: 'firstPage',
      firstPageUrl: 'firstPageUrl',
      lastPageUrl: 'lastPageUrl',
      nextPageUrl: 'nextPageUrl',
      previousPageUrl: 'previousPageUrl',
    }
  }
}
