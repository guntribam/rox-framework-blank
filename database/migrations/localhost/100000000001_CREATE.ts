import Database from '@ioc:Adonis/Lucid/Database'
import BaseSchema from '@ioc:Adonis/Lucid/Schema'
import Env from '@ioc:Adonis/Core/Env'

export default class CreateDB extends BaseSchema {
  public async up() {
    await Database.rawQuery(`
      IF EXISTS (SELECT [name] FROM master.sys.databases WHERE [name] = N'${Env.get('MSSQL_CORPORATIVO_DB_NAME')}') DROP DATABASE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}
      IF NOT EXISTS (SELECT [name] FROM sys.databases WHERE [name] = N'${Env.get('MSSQL_CORPORATIVO_DB_NAME')}') CREATE DATABASE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}

      IF EXISTS (SELECT [name] FROM master.sys.databases WHERE [name] = N'${Env.get('MSSQL_APP_DB_NAME')}') DROP DATABASE ${Env.get('MSSQL_APP_DB_NAME')}
      IF NOT EXISTS (SELECT [name] FROM sys.databases WHERE [name] = N'${Env.get('MSSQL_APP_DB_NAME')}') CREATE DATABASE ${Env.get('MSSQL_APP_DB_NAME')}
    `)
  }

  public async down() {}
}
