import BaseSchema from '@ioc:Adonis/Lucid/Schema'
import Env from '@ioc:Adonis/Core/Env'

export default class Corporativo extends BaseSchema {
  public async up() {
    this.schema.raw(`
    SET ANSI_NULLS ON
    SET QUOTED_IDENTIFIER ON
    SET ANSI_PADDING ON

    CREATE TABLE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.[dbo].[ORGAO](
      [iCodigo] [int] IDENTITY(1,1) NOT NULL,
      [iNumero] [int] NULL,
      [sSigla] [char](50) NULL,
      [sNome] [varchar](500) NOT NULL,
      [sNomeExtendido] [varchar](500) NULL,
      [sEmail] [varchar](50) NULL,
      [iTipo] [smallint] NULL,
      [iNatureza] [smallint] NULL,
      [iCircunscricao] [smallint] NULL,
      [iResponsavel] [int] NULL,
      [tAtribuicao] [text] NULL,
      [cAtividade] [char](1) NULL,
      [cStatus] [char](1) NULL,
      [cAmbito] [char](1) NULL,
      [iFiltro] [int] NULL,
      [tAssuntos] [text] NULL,
      [dtCriacao] [datetime] NULL,
      [msrepl_tran_version] [uniqueidentifier] NULL,
      [iGrupo] [int] NULL,
      [iIdRh] [int] NULL,
      [cEstatistica] [char](1) NULL,
     CONSTRAINT [pkOrgao] PRIMARY KEY CLUSTERED
    (
      [iCodigo] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]



    CREATE TABLE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.[dbo].[PESSOA](
      [iCodigo] [int] IDENTITY(1,1) NOT NULL,
      [iMatricula] [int]  NULL,
      [sNome] [varchar](80) NOT NULL,
      [iIdRede] [int] NULL,
      [iTipo] [smallint]  NULL,
      [sIdentidade] [char](20) NULL,
      [sOrgaoExpedIdentidade] [char](10) NULL,
      [nCpf] [decimal](11, 0) NULL,
      [sNomePai] [varchar](80) NULL,
      [sNomeMae] [varchar](80) NULL,
      [dtNascimento] [datetime] NULL,
      [iCargo] [smallint] NULL,
      [dtNomeacao] [datetime] NULL,
      [dtPosse] [datetime] NULL,
      [dtExercicio] [datetime] NULL,
      [dtProgressaoPromotor] [datetime] NULL,
      [cMotivoProgressaoPromotor] [char](1) NULL,
      [dtProgressaoProcurador] [datetime] NULL,
      [cMotivoProgressaoProcurador] [char](1) NULL,
      [dtModificacao] [datetime]  NULL,
      [bFoto] [image] NULL,
      [iFiltro] [int]  NULL,
      [iNivelEscolaridade] [smallint] NULL,
      [cNivelEscolaridadeCompleto] [char](1) NULL,
      [iEstadoCivil] [smallint] NULL,
      [sNomeConjuge] [varchar](80) NULL,
      [sNaturalidade] [varchar](80) NULL,
      [sUfNaturalidade] [char](2) NULL,
      [sNacionalidade] [varchar](80) NULL,
      [iNacionalidade] [smallint] NULL,
      [nPisPasep] [decimal](11, 0) NULL,
      [msrepl_tran_version] [uniqueidentifier]  NULL,
      [iDigitoVerificador] [int] NULL,
      [cSexo] [varchar](1) NULL,
      [iTipoSanguineo] [int] NULL,
      [dtExpedIdentidade] [datetime] NULL,
     CONSTRAINT [pkPessoa] PRIMARY KEY CLUSTERED
    (
      [iCodigo] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]



    CREATE TABLE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.[dbo].[SISTEMA](
      [iCodigo] [smallint] IDENTITY(1,1) NOT NULL,
      [sNome] [varchar](30) NOT NULL,
      [sDescricao] [varchar](50) NULL,
     CONSTRAINT [PK_SISTEMA] PRIMARY KEY CLUSTERED
    (
      [iCodigo] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
     CONSTRAINT [UN_NOME_SISTEMA] UNIQUE NONCLUSTERED
    (
      [sNome] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY]


    CREATE TABLE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.[dbo].[USUARIO_REDE](
      [iRID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
      [sLogin] [char](32) NOT NULL,
      [sEmail] [varchar](64) NULL,
      [dtModificacao] [datetime]  NULL,
      [msrepl_tran_version] [uniqueidentifier]  NULL,
      [iSecurityId] [binary](16) NULL,
     CONSTRAINT [pkUsuarioRede] PRIMARY KEY CLUSTERED
    (
      [iRID] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
     CONSTRAINT [unLoginUsuario] UNIQUE NONCLUSTERED
    (
      [sLogin] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]


    ALTER TABLE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.[dbo].[USUARIO_REDE] ADD  CONSTRAINT [dfModificacaoUsuario]  DEFAULT (getdate()) FOR [dtModificacao]

    ALTER TABLE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.[dbo].[USUARIO_REDE] ADD  DEFAULT (newid()) FOR [msrepl_tran_version]
    ALTER TABLE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.[dbo].[ORGAO]  WITH NOCHECK ADD  CONSTRAINT [fkResponsavelOrgao] FOREIGN KEY([iResponsavel])
    REFERENCES ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.[dbo].[PESSOA] ([iCodigo])

    ALTER TABLE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.[dbo].[ORGAO] CHECK CONSTRAINT [fkResponsavelOrgao]

    CREATE TABLE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.[dbo].[T_CIRCUNSCRICAO](
      [iCodigo] [smallint] NOT NULL,
      [sNome] [varchar](100) NULL,
      [msrepl_tran_version] [uniqueidentifier] NOT NULL DEFAULT (newid()),
      [sSigla] [varchar](4) NULL,
     CONSTRAINT [pkCircunscricao] PRIMARY KEY CLUSTERED
    (
      [iCodigo] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
     CONSTRAINT [unNomeCircunscricao] UNIQUE NONCLUSTERED
    (
      [sNome] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]


    CREATE TABLE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.[dbo].[T_TIPO_PESSOA](
      [iCodigo] [smallint] IDENTITY(1,1) NOT NULL,
      [sDescricao] [varchar](100) NULL,
      [cAtividade] [char](1) NOT NULL,
      [iUsuarioRede] [smallint] NOT NULL,
      [cStatus] [char](1) NULL,
      [msrepl_tran_version] [uniqueidentifier] NOT NULL,
      [sContainerActiveDiretory] [varchar](255) NULL,
      [iPrioridadeCadastro] [int] NULL,
      [iManterGrupos] [tinyint] NULL,
     CONSTRAINT [pkTipoPessoa] PRIMARY KEY CLUSTERED
    (
      [iCodigo] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
     CONSTRAINT [unDescPessoa] UNIQUE NONCLUSTERED
    (
      [sDescricao] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY]


    ALTER TABLE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.[dbo].[T_TIPO_PESSOA]  WITH NOCHECK ADD  CONSTRAINT [ckAtivo] CHECK  (([cAtividade] = 'I' or [cAtividade] = 'A'))

    ALTER TABLE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.[dbo].[T_TIPO_PESSOA] CHECK CONSTRAINT [ckAtivo]

    ALTER TABLE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.[dbo].[T_TIPO_PESSOA]  WITH NOCHECK ADD  CONSTRAINT [ckStatusTipoPessoa] CHECK  (([cStatus] is null or ([cStatus] = 'F' or ([cStatus] = 'P' or ([cStatus] = 'I' or [cStatus] = 'A')))))

    ALTER TABLE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.[dbo].[T_TIPO_PESSOA] CHECK CONSTRAINT [ckStatusTipoPessoa]

    ALTER TABLE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.[dbo].[T_TIPO_PESSOA]  WITH NOCHECK ADD  CONSTRAINT [ckUsuarioRede] CHECK  (([iUsuarioRede] = 3 or ([iUsuarioRede] = 2 or ([iUsuarioRede] = 1 or [iUsuarioRede] = 0))))

    ALTER TABLE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.[dbo].[T_TIPO_PESSOA] CHECK CONSTRAINT [ckUsuarioRede]

    ALTER TABLE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.[dbo].[T_TIPO_PESSOA] ADD  CONSTRAINT [dfAtivo]  DEFAULT ('I') FOR [cAtividade]

    ALTER TABLE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.[dbo].[T_TIPO_PESSOA] ADD  CONSTRAINT [dfUsuarioRede]  DEFAULT (0) FOR [iUsuarioRede]

    ALTER TABLE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.[dbo].[T_TIPO_PESSOA] ADD  CONSTRAINT [dfStatusTipoPessoa]  DEFAULT ('F') FOR [cStatus]

    ALTER TABLE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.[dbo].[T_TIPO_PESSOA] ADD  DEFAULT (newid()) FOR [msrepl_tran_version]

    ALTER TABLE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.[dbo].[T_TIPO_PESSOA] ADD  CONSTRAINT [T_TIPO_PESSOA_iManterGrupos_default]  DEFAULT ('1') FOR [iManterGrupos]

    if exists (select 1
       from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
       where r.fkeyid = object_id('${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACAO') and o.name = 'fkAcao_AcaoPai')
    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACAO
       drop constraint fkAcao_AcaoPai

    if exists (select 1
       from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
       where r.fkeyid = object_id('${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACAO') and o.name = 'fkAcao_Sistema')
    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACAO
       drop constraint fkAcao_Sistema

    if exists (select 1
       from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
       where r.fkeyid = object_id('${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ACAO') and o.name = 'fkAcessoAcao_Acao')
    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ACAO
       drop constraint fkAcessoAcao_Acao

    if exists (select 1
       from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
       where r.fkeyid = object_id('${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ACAO') and o.name = 'fkAcessoAcao_AcessoOrgao')
    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ACAO
       drop constraint fkAcessoAcao_AcessoOrgao

    if exists (select 1
       from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
       where r.fkeyid = object_id('${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ORGAO') and o.name = 'fkAcessoOrgao_Orgao')
    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ORGAO
       drop constraint fkAcessoOrgao_Orgao

    if exists (select 1
       from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
       where r.fkeyid = object_id('${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ORGAO') and o.name = 'fkAcessoOrgao_Pessoa')
    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ORGAO
       drop constraint fkAcessoOrgao_Pessoa

    if exists (select 1
       from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
       where r.fkeyid = object_id('${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ORGAO') and o.name = 'fkAcessoOrgao_Sistema')
    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ORGAO
       drop constraint fkAcessoOrgao_Sistema

    if exists (select 1
       from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
       where r.fkeyid = object_id('${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ORGAO') and o.name = 'fkAcessoOrgao_TipoSigilo')
    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ORGAO
       drop constraint fkAcessoOrgao_TipoSigilo

    if exists (select 1
       from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
       where r.fkeyid = object_id('${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.MENU') and o.name = 'fkMenu_Acao')
    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.MENU
       drop constraint fkMenu_Acao

    if exists (select 1
       from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
       where r.fkeyid = object_id('${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_ACAO') and o.name = 'fkPapelAcao_Acao')
    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_ACAO
       drop constraint fkPapelAcao_Acao

    if exists (select 1
       from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
       where r.fkeyid = object_id('${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_ACAO') and o.name = 'fkPapelAcao_PapelAcesso')
    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_ACAO
       drop constraint fkPapelAcao_PapelAcesso

    if exists (select 1
       from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
       where r.fkeyid = object_id('${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_ACESSO') and o.name = 'fkPapelAcesso_Sistema')
    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_ACESSO
       drop constraint fkPapelAcesso_Sistema

    if exists (select 1
       from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
       where r.fkeyid = object_id('${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_PESSOA') and o.name = 'fkPapelPessoa_PapelAcesso')
    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_PESSOA
       drop constraint fkPapelPessoa_PapelAcesso

    if exists (select 1
       from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
       where r.fkeyid = object_id('${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_PESSOA_ORGAO') and o.name = 'fkPapelPessoaOrgao_AcessoOrgao')
    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_PESSOA_ORGAO
       drop constraint fkPapelPessoaOrgao_AcessoOrgao

    if exists (select 1
       from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
       where r.fkeyid = object_id('${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_PESSOA_ORGAO') and o.name = 'fkPapelPessoaOrgao_PapelAcesso')
    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_PESSOA_ORGAO
       drop constraint fkPapelPessoaOrgao_PapelAcesso

    if exists (select 1
                from  sysobjects
               where  id = object_id('${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACAO')
                and   type = 'U')
       drop table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACAO

    --if exists (select 1
    --            from  sysindexes
    --           where  id    = object_id('${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ORGAO')
    --            and   name  = 'fkAcessoOrgao_Pessoa'
    --            and   indid > 0
    --            and   indid < 255)
    --   drop index ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ORGAO.fkAcessoOrgao_Pessoa

    if exists (select 1
                from  sysobjects
               where  id = object_id('${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ORGAO')
                and   type = 'U')
       drop table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ORGAO

    if exists (select 1
                from  sysobjects
               where  id = object_id('${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_ACAO')
                and   type = 'U')
       drop table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_ACAO

    if exists (select 1
                from  sysobjects
               where  id = object_id('${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_ACESSO')
                and   type = 'U')
       drop table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_ACESSO

    if exists (select 1
                from  sysobjects
               where  id = object_id('${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_PESSOA_ORGAO')
                and   type = 'U')
       drop table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_PESSOA_ORGAO

    if exists (select 1
                from  sysobjects
               where  id = object_id('${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.TIPO_SIGILO')
                and   type = 'U')
       drop table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.TIPO_SIGILO


    /*==============================================================*/
    /* Table: ACAO                                                  */
    /*==============================================================*/
    create table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACAO (
       idAcao               int                  identity,
       nmAcao               varchar(50)          not null,
       idSistema            smallint             not null,
       stExigirDelegacao    char(1)              not null constraint dfAcao_stExigirDelegacao default 'N'
          constraint ckAcao_stExigirDelegacao check (stExigirDelegacao in ('S','N')),
       txResultado          varchar(200)         null,
       idAcaoPai            int                  null,
       nrOrdemExibicao      int                  null,
       stAtivo              char(1)              not null constraint dfAcao_stAtivo default 'S'
          constraint ckAcao_stAtivo check (stAtivo in ('S','N')),
       stRequerAcesso       char(1)              not null constraint dfAcao_stRequerAcesso default 'N'
          constraint ckAcao_stRequerAcesso check (stRequerAcesso in ('S','N')),
       tpNaturezaAcao       smallint             null
          constraint ckAcao_NaturezaAcao check (tpNaturezaAcao is null or (tpNaturezaAcao in (1,2))),
       constraint pkAcao primary key nonclustered (idAcao)
             on "PRIMARY",
       constraint uxAcao_SistemaNome unique (idSistema, nmAcao)
             on "PRIMARY",
       constraint uxAcao_Sistema unique (idSistema, idAcao)
             on "PRIMARY"
    )
    on "PRIMARY"

    /*==============================================================*/
    /* Table: ACESSO_ORGAO                                          */
    /*==============================================================*/
    create table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ORGAO (
       idAcessoOrgao        int                  identity(1, 1),
       idSistema            smallint             not null,
       idPessoa             int                  not null,
       idOrgao              int                  not null,
       idTipoSigilo         int                  not null,
       constraint pkAcessoOrgao primary key nonclustered (idAcessoOrgao)
             on "PRIMARY",
       constraint uxAcessoOrgao_SistemaPessoaOrgao unique (idSistema, idPessoa, idOrgao)
             on "PRIMARY",
       constraint uxAcessoOrgao_Sistema unique (idSistema, idAcessoOrgao)
             on "PRIMARY"
    )
    on "PRIMARY"

    /*==============================================================*/
    /* Index: fkAcessoOrgao_Pessoa                                  */
    /*==============================================================*/
    create index fkAcessoOrgao_Pessoa on ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ORGAO (
      idPessoa ASC
    )
    on "PRIMARY"

    /*==============================================================*/
    /* Table: PAPEL_ACAO                                            */
    /*==============================================================*/
    create table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_ACAO (
       idPapelAcao          int                  identity,
       idSistema            smallint             not null,
       idPapelAcesso        int                  not null,
       idAcao               int                  not null,
       stAtivo              char(1)              not null constraint dfPapelAcao_stAtivo default 'S'
          constraint ckPapelAcao_stAtivo check (stAtivo in ('S','N')),
       constraint pkPapelAcao primary key nonclustered (idPapelAcao)
             on "PRIMARY",
       constraint uxPapelAcao unique (idPapelAcesso, idAcao)
    )
    on "PRIMARY"

    /*==============================================================*/
    /* Table: PAPEL_ACESSO                                          */
    /*==============================================================*/
    create table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_ACESSO (
       idPapelAcesso        int                  identity,
       nmPapelAcesso        varchar(30)          not null,
       idSistema            smallint             not null,
       constraint pkPapelAcesso primary key nonclustered (idPapelAcesso)
             on "PRIMARY",
       constraint uxPapelAcesso_Sistema unique (idSistema, idPapelAcesso),
       constraint uxPapelAcesso_SistemaNome unique (nmPapelAcesso, idSistema)
    )
    on "PRIMARY"

    /*==============================================================*/
    /* Table: PAPEL_PESSOA_ORGAO                                    */
    /*==============================================================*/
    create table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_PESSOA_ORGAO (
       idPapelPessoaOrgao   int                  identity,
       idSistema            smallint             not null,
       idPapelAcesso        int                  not null,
       idAcessoOrgao        int                  not null,
       constraint pkPapelPessoaOrgao primary key nonclustered (idPapelPessoaOrgao)
             on "PRIMARY",
       constraint uxPapelPessoaOrgao unique (idAcessoOrgao, idPapelAcesso)
    )
    on "PRIMARY"

    /*==============================================================*/
    /* Table: TIPO_SIGILO                                           */
    /*==============================================================*/
    create table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.TIPO_SIGILO (
       idTipoSigilo         int                  identity(0,1),
       nmTipoSigilo         varchar(30)          not null,
       constraint pkTipoSigilo primary key nonclustered (idTipoSigilo)
             on "PRIMARY",
       constraint uxTipoSigilo_Nome unique (nmTipoSigilo)
    )
    on "PRIMARY"

    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACAO
       add constraint fkAcao_AcaoPai foreign key (idSistema, idAcaoPai)
          references ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACAO (idSistema, idAcao)

    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACAO
       add constraint fkAcao_Sistema foreign key (idSistema)
          references ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.SISTEMA (iCodigo)

    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ORGAO
       add constraint fkAcessoOrgao_Orgao foreign key (idOrgao)
          references ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ORGAO (iCodigo)

    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ORGAO
       add constraint fkAcessoOrgao_Pessoa foreign key (idPessoa)
          references ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PESSOA (iCodigo)

    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ORGAO
       add constraint fkAcessoOrgao_Sistema foreign key (idSistema)
          references ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.SISTEMA (iCodigo)

    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ORGAO
       add constraint fkAcessoOrgao_TipoSigilo foreign key (idTipoSigilo)
          references ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.TIPO_SIGILO (idTipoSigilo)

    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_ACAO
       add constraint fkPapelAcao_Acao foreign key (idSistema, idAcao)
          references ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACAO (idSistema, idAcao)

    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_ACAO
       add constraint fkPapelAcao_PapelAcesso foreign key (idSistema, idPapelAcesso)
          references ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_ACESSO (idSistema, idPapelAcesso)

    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_ACESSO
       add constraint fkPapelAcesso_Sistema foreign key (idSistema)
          references ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.SISTEMA (iCodigo)

    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_PESSOA_ORGAO
       add constraint fkPapelPessoaOrgao_AcessoOrgao foreign key (idSistema, idAcessoOrgao)
          references ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.ACESSO_ORGAO (idSistema, idAcessoOrgao)

    alter table ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_PESSOA_ORGAO
       add constraint fkPapelPessoaOrgao_PapelAcesso foreign key (idSistema, idPapelAcesso)
          references ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}.dbo.PAPEL_ACESSO (idSistema, idPapelAcesso)
    `)
  }

  public async down() {}
}
