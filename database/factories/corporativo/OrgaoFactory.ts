import Factory from '@ioc:Adonis/Lucid/Factory'
import type Circunscricao from 'App/_corporativo/circunscricao/Circunscricao'
import Orgao from 'App/_corporativo/orgao/Orgao'

export const OrgaoFactory = Factory.define(Orgao, async ({ faker }) => ({
  nome: faker.company.companyName(),
  sigla: faker.address.cityName(),
  temEstatistica: false,
  atividade: 'O',
  status: 'A',
  ambito: 'I',
  filtro: 0,
  msreplTranVersion: 'B9A09751-F878-4801-8149-02F8141D4A19',
})).build()

interface OrgaoBuilder {
  nome: string
  nomeExtendido: string
  sigla: string
  circunscricao?: Circunscricao
}

export async function orgao({ nome, nomeExtendido, sigla, circunscricao }: OrgaoBuilder): Promise<Orgao> {
  const orgao = await OrgaoFactory.merge({ nome, nomeExtendido, sigla }).create()
  if (circunscricao) await orgao.related('circunscricao').associate(circunscricao)
  return orgao
}
