import Factory from '@ioc:Adonis/Lucid/Factory'
import Circunscricao from 'App/_corporativo/circunscricao/Circunscricao'

export const CircunscricaoFactory = Factory.define(Circunscricao, async ({ faker }) => ({
  id: faker.datatype.number(100),
  nome: faker.address.cityName(),
  sigla: faker.address.cityName(),
})).build()

interface CircunscricaoBuilder {
  id: number
  nome: string
  sigla: string
}

export async function circunscricao({
  id,
  nome,
  sigla,
}: CircunscricaoBuilder): Promise<Circunscricao> {
  return await CircunscricaoFactory.merge({ id, nome, sigla }).create()
}
