import Factory from '@ioc:Adonis/Lucid/Factory'
import type AcessoOrgao from 'App/_corporativo/acessoorgao/AcessoOrgao'
import type PapelAcesso from 'App/_corporativo/papelacesso/PapelAcesso'
import PapelPessoaOrgao from 'App/_corporativo/papelpessoaorgao/PapelPessoaOrgao'
import type Sistema from 'App/_corporativo/sistema/Sistema'
import { AcessoOrgaoFactory } from './AcessoOrgaoFactory'
import { PapelAcessoFactory } from './PapelAcessoFactory'
import { SistemaFactory } from './SistemaFactory'

export const PapelPessoaOrgaoFactory = Factory.define(PapelPessoaOrgao, async () => ({}))
  .relation('sistema', SistemaFactory)
  .relation('acessoOrgao', AcessoOrgaoFactory)
  .relation('papelAcesso', PapelAcessoFactory)
  .build()

type PapelPessoaOrgaoType = {
  papel: PapelAcesso
  acessoOrgao: AcessoOrgao
  sistema: Sistema
}
export async function papelPessoaOrgao(ppo: PapelPessoaOrgaoType): Promise<PapelPessoaOrgao> {
  return await PapelPessoaOrgaoFactory.merge({
    idPapelAcesso: ppo.papel.id,
    idAcessoOrgao: ppo.acessoOrgao.id,
    idSistema: ppo.sistema.id,
  }).create()
}
