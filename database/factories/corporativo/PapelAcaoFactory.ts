import Factory from '@ioc:Adonis/Lucid/Factory'
import type Acao from 'App/_corporativo/acao/Acao'
import PapelAcao from 'App/_corporativo/papelacao/PapelAcao'
import type PapelAcesso from 'App/_corporativo/papelacesso/PapelAcesso'
import type Sistema from 'App/_corporativo/sistema/Sistema'
import { SistemaFactory } from './SistemaFactory'

export const PapelAcaoFactory = Factory.define(PapelAcao, async () => ({}))
  .relation('sistema', SistemaFactory)
  .build()

type PapelAcaoType = {
  papel: PapelAcesso
  acoes: Acao[]
  sistema: Sistema
}

export async function papelAcao({ papel, acoes, sistema }: PapelAcaoType): Promise<PapelAcao[]> {
    const papeisComAcoes: PapelAcao[] = []
    for (const acao of acoes) {
      papeisComAcoes.push(await PapelAcaoFactory.merge({
        idPapelAcesso: papel.id,
        idAcao: acao.id,
        idSistema: sistema.id,
        ativo: true
      }).create())
    }

  return papeisComAcoes;
}
