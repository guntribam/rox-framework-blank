import Factory from '@ioc:Adonis/Lucid/Factory'
import Pessoa from 'App/_corporativo/pessoa/Pessoa'
import TipoPessoaEnum from 'App/_corporativo/tipopessoa/TipoPessoaEnum'
import UsuarioRede from 'App/_corporativo/usuariorede/UsuarioRede'
import { date } from '../FactoryUtils'
import { OrgaoFactory } from './OrgaoFactory'

export const UsuarioRedeFactory = Factory.define(UsuarioRede, ({ faker }) => ({
  login: faker.internet.userName(),
  email: 'email@email.com',
}))
  .relation('pessoa', () => PessoaFactory)
  .build()

export const PessoaFactory = Factory.define(Pessoa, async ({ faker }) => ({
  nome: `${faker.name.firstName()} ${faker.name.lastName()}`,
  matricula: 1,
  dataModificacao: date('2021-01-01', '2021-06-01'),
  idTipoPessoa: TipoPessoaEnum.QUADRO_PERMANENTE.id,
  filtro: 0,
  msreplTranVersion: '2A14FCA0-30D9-4BDE-B153-49B6EA580A5B',
}))
  .relation('usuario', () => UsuarioRedeFactory)
  .relation('orgaos', () => OrgaoFactory)
  .build()

export interface PessoaBuilder {
  nome: string
  login: string
  tipo?: `${keyof typeof TipoPessoaEnum}`
}

export async function pessoa({ nome, login, tipo }: PessoaBuilder) {
  const dados = {
    nome,
    ...(tipo && { idTipoPessoa: TipoPessoaEnum[tipo].id }),
  }
  return await PessoaFactory.merge(dados)
    .with('usuario', 1, (u) => u.merge({ login }))
    .create()
}
