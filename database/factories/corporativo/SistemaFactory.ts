import Factory from "@ioc:Adonis/Lucid/Factory"
import Sistema from "App/_corporativo/sistema/Sistema"

export const SistemaFactory = Factory.define(Sistema, async ({ faker }) => ({
  nome: faker.name.firstName(),
  descricao: faker.lorem.word(),
}))
  .build()


export async function sistema({
  nome,
  descricao,
}: {
  nome: string
  descricao?: string
}): Promise<Sistema> {
  return await SistemaFactory.merge({ nome, descricao: descricao ?? nome }).create()
}
