import Factory from '@ioc:Adonis/Lucid/Factory'
import AcessoOrgao from 'App/_corporativo/acessoorgao/AcessoOrgao'
import type Orgao from 'App/_corporativo/orgao/Orgao'
import type Pessoa from 'App/_corporativo/pessoa/Pessoa'
import type Sistema from 'App/_corporativo/sistema/Sistema'
import { OrgaoFactory } from './OrgaoFactory'
import { PessoaFactory } from './PessoaFactory'
import { SistemaFactory } from './SistemaFactory'

export const AcessoOrgaoFactory = Factory.define(AcessoOrgao, async () => ({}))
  .relation('sistema', SistemaFactory)
  .relation('pessoa', PessoaFactory)
  .relation('orgao', OrgaoFactory)
  .build()

type AcessoType = {
  pessoa: Pessoa
  orgao: Orgao
  sistema: Sistema
}

export async function acessoOrgao({ pessoa, orgao, sistema }: AcessoType): Promise<AcessoOrgao> {
  return await AcessoOrgaoFactory.merge({
    idPessoa: pessoa.id,
    idOrgao: orgao.id,
    idSistema: sistema.id,
    idTipoSigilo: 0,
  }).create()
}
