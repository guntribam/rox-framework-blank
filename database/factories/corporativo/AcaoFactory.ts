import Factory from '@ioc:Adonis/Lucid/Factory'
import Acao from 'App/_corporativo/acao/Acao'
import type Sistema from 'App/_corporativo/sistema/Sistema'
import { SistemaFactory } from './SistemaFactory'

export const AcaoFactory = Factory.define(Acao, async () => ({}))
  .relation('sistema', SistemaFactory)
  .build()

type AcaoType = {
  nome: string
  sistema: Sistema
}

export async function acao({ nome, sistema }: AcaoType): Promise<Acao> {
  return await AcaoFactory.merge({
    nome,
    idSistema: sistema.id,
    ativo: true,
    exigirDelegacao: false,
    requerAcesso: false
  }).create()
}
