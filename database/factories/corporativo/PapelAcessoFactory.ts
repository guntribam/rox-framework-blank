import Factory from '@ioc:Adonis/Lucid/Factory'
import PapelAcesso from 'App/_corporativo/papelacesso/PapelAcesso'
import type Sistema from 'App/_corporativo/sistema/Sistema'
import { SistemaFactory } from './SistemaFactory'

export const PapelAcessoFactory = Factory.define(PapelAcesso, async () => ({}))
  .relation('sistema', SistemaFactory)
  .build()

type PapelAcessoType = {
  nome: string
  sistema: Sistema
}
export async function papelAcesso({ nome, sistema }: PapelAcessoType): Promise<PapelAcesso> {
  return await PapelAcessoFactory.merge({ nome, idSistema: sistema.id }).create()
}
