import Database from '@ioc:Adonis/Lucid/Database'
import faker from '@faker-js/faker'
import { DateTime } from 'luxon'

type d = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 0
type YYYY = `19${d}${d}` | `20${d}${d}`
type oneToNine = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9
type MM = `0${oneToNine}` | `1${0 | 1 | 2}`
type DD = `${0}${oneToNine}` | `${1 | 2}${d}` | `3${0 | 1}`
export type DateFormat = `${YYYY}-${MM}-${DD}`
export type DateFormatForTests = `${DD}/${MM}/${YYYY}`

export const date = (from: DateFormat, to: DateFormat): DateTime =>
  DateTime.fromJSDate(faker.date.between(from, to))

export const criartabelasTipo = async (tables: any[]) => {
  for (const { model, enumType } of tables) {
    const rows: any[] = Object.values(enumType)
    const notInserted = (await model.findMany(rows.map((r: any) => r.id))).length !== rows.length
    if (notInserted) {
      await Database.rawQuery(`
          SET IDENTITY_INSERT ${model.table} ON

          INSERT INTO ${model.table}
          (${Object.keys(rows[0])
            .map((key) => model.$columnsDefinitions.get(key)?.columnName)
            .join(',')})
          VALUES
          ${rows.map((row) => '(' + Object.values(row).map((v) => "'" + v + "'") + ')').join(',')}

          SET IDENTITY_INSERT ${model.table} OFF
        `)
    }
  }
}
