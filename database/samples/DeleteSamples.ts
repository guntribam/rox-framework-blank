import Database from '@ioc:Adonis/Lucid/Database'
import Env from '@ioc:Adonis/Core/Env'

export const cleanTableData = async () => {
  await Database.knexRawQuery(`
    USE ${Env.get('MSSQL_CORPORATIVO_DB_NAME')}
    EXEC sys.sp_MSForEachTable 'ALTER TABLE \\? NOCHECK CONSTRAINT ALL'
    EXEC sys.sp_MSForEachTable 'DELETE FROM \\?'
    EXEC sys.sp_MSForEachTable 'ALTER TABLE \\? CHECK CONSTRAINT ALL'
    EXEC sys.sp_MSForEachTable 'IF OBJECTPROPERTY(object_id(''\\?''), ''TableHasIdentity'') = 1 DBCC CHECKIDENT (''\\?'', RESEED, 0)'

    USE ${Env.get('MSSQL_APP_DB_NAME')}
    EXEC sys.sp_MSForEachTable 'ALTER TABLE \\? NOCHECK CONSTRAINT ALL'
    EXEC sys.sp_MSForEachTable 'DELETE FROM \\?'
    EXEC sys.sp_MSForEachTable 'ALTER TABLE \\? CHECK CONSTRAINT ALL'
    EXEC sys.sp_MSForEachTable 'IF OBJECTPROPERTY(object_id(''\\?''), ''TableHasIdentity'') = 1 DBCC CHECKIDENT (''\\?'', RESEED, 0)'
  `)
}
