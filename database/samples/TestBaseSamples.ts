import { appBaseData } from "./AppSamples";
import { corporativoBaseData } from "./corporativo/CorporativoSamples"
import { cache } from "./SamplesUtils"

export const testBaseData = cache(async () => {
  await corporativoBaseData();
  await appBaseData();
})
