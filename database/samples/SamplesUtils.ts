let _cache = {}
let _cacheBaseState = {}
export function cache<T>(callback: () => Promise<T>): () => Promise<T> {
  return async () => {
    const key = callback.toString()
    if (!_cache[key]) {
      _cache[key] = callback()
    }
    return _cache[key]
  }
}

export const initCacheBaseState = () => _cacheBaseState = _cache
export const restoreCacheBaseState = () => _cache = _cacheBaseState
export const resetCache = () => _cache = {}
