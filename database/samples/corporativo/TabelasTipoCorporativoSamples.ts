import TipoPessoa from 'App/_corporativo/tipopessoa/TipoPessoa'
import TipoPessoaEnum from 'App/_corporativo/tipopessoa/TipoPessoaEnum'
import TipoSigilo from 'App/_corporativo/tiposigilo/TipoSigilo'
import TipoSigiloEnum from 'App/_corporativo/tiposigilo/TipoSigiloEnum'
import { criartabelasTipo } from '../../factories/FactoryUtils'

export const tabelasTipoCorporativo = async () => {
  await criartabelasTipo([
    { model: TipoPessoa, enumType: TipoPessoaEnum },
    { model: TipoSigilo, enumType: TipoSigiloEnum }
  ])
}
