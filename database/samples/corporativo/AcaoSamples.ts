import { papelAcao } from 'Database/factories/corporativo/PapelAcaoFactory'
import { cache } from '../SamplesUtils'
import { chefe, funcionario } from './PapelAcessoSamples'
import { app } from './SistemaSamples'

export const criarPapeisComAcoes = cache(async () => {
  await papelAcao({
    sistema: await app(),
    papel: await chefe(),
    acoes: await acoesChefe(),
  })
  await papelAcao({
    sistema: await app(),
    papel: await funcionario(),
    acoes: await acoesFuncionario(),
  })
})

const acoesChefe = async () => [...(await acoesFuncionario())]

const acoesFuncionario = async () => [
]
