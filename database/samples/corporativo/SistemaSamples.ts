import { sistema } from "Database/factories/corporativo/SistemaFactory";
import { cache } from "../SamplesUtils";

export const app = cache(async () => await sistema({ nome: '::NOME_SISTEMA::' }))
