import { orgao } from 'Database/factories/corporativo/OrgaoFactory'
import { cache } from '../SamplesUtils'
import { circunscricaoBSB } from './CircunscricaoSamples'

export const pgj = cache(
  async () =>
    await orgao({
      nome: 'Procuradoria-Geral de Justiça do Ministério Público do Distrito Federal e Territórios',
      nomeExtendido:
        'Procuradoria-Geral de Justiça do Ministério Público do Distrito Federal e Territórios',
      sigla: 'PGJ',
      circunscricao: await circunscricaoBSB(),
    })
)

export const pjCriminalCeilandia1 = cache(
  async () =>
    await orgao({
      nome: '01a. P.J. Criminal de Ceilândia',
      nomeExtendido: '01a. P.J. Criminal de Ceilândia',
      sigla: '1ªPJCRI-CE',
      circunscricao: await circunscricaoBSB(),
    })
)

export const STI = cache(
  async () =>
    await orgao({
      nome: 'Secretaria de tecnologia da informação',
      nomeExtendido: 'Secretaria de tecnologia da informação',
      sigla: 'STI',
      circunscricao: await circunscricaoBSB(),
    })
)
