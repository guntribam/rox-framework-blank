import { arinda, danilo, renato } from './PessoaSamples'
import { tabelasTipoCorporativo } from './TabelasTipoCorporativoSamples'
import { cache } from '../SamplesUtils'
import { criarPapeisComAcoes } from './AcaoSamples'

export const corporativoBaseData = cache(async () => {
  await tabelasTipoCorporativo()
  await criarPapeisComAcoes()
  await arinda()
  await renato()
  await danilo()
})
