import type Orgao from 'App/_corporativo/orgao/Orgao'
import type PapelAcesso from 'App/_corporativo/papelacesso/PapelAcesso'
import type Sistema from 'App/_corporativo/sistema/Sistema'
import { acessoOrgao } from 'Database/factories/corporativo/AcessoOrgaoFactory'
import { papelPessoaOrgao } from 'Database/factories/corporativo/PapelPessoaOrgaoFactory'
import {
  pessoa,
  PessoaBuilder
} from 'Database/factories/corporativo/PessoaFactory'
import { cache } from '../SamplesUtils'
import { pgj, pjCriminalCeilandia1, STI } from './OrgaoSamples'
import { chefe, funcionario } from './PapelAcessoSamples'
import { app } from './SistemaSamples'

export const arinda = cache(
  async () =>
    await pessoaBuilder({
      nome: 'ARINDA FERNANDES',
      login: 'Arinda',
      tipo: 'MEMBRO_ATIVO',
      papeisPorOrgao: [
        { orgao: await pjCriminalCeilandia1(), papeis: [await chefe(), await funcionario()] },
        { orgao: await pgj(), papeis: [await funcionario()] },
      ],
      sistema: await app(),
    })
)

export const renato = cache(
  async () =>
    await pessoaBuilder({
      nome: 'RENATO SILVA',
      login: 'renato.silva',
      tipo: 'QUADRO_PERMANENTE',
      papeisPorOrgao: [{ orgao: await STI(), papeis: [await chefe()] }],
      sistema: await app(),
    })
)

export const danilo = cache(
  async () =>
    await pessoaBuilder({
      nome: 'DANILO CARNEIRO',
      login: 'danilo.carneiro',
      tipo: 'QUADRO_PERMANENTE',
      papeisPorOrgao: [{ orgao: await STI(), papeis: [await funcionario()] }],
      sistema: await app(),
    })
)


type PessoaMPDFTBuilder = PessoaBuilder & {
  papeisPorOrgao: { orgao: Orgao; papeis: PapelAcesso[] }[]
  sistema: Sistema
}

async function pessoaBuilder({ nome, login, tipo, papeisPorOrgao, sistema }: PessoaMPDFTBuilder) {
  const _pessoa = await pessoa({ nome, login, tipo })
  for (const orgao of papeisPorOrgao) {
    const _acessoOrgao = await acessoOrgao({ pessoa: _pessoa, orgao: orgao.orgao, sistema })
    for (const papel of orgao.papeis) {
      await papelPessoaOrgao({
        papel,
        acessoOrgao: _acessoOrgao,
        sistema,
      })
    }
  }
  return _pessoa
}
