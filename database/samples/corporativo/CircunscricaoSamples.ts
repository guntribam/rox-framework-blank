import { circunscricao } from "Database/factories/corporativo/CircunscricaoFactory";
import { cache } from "../SamplesUtils";

export const circunscricaoBSB = cache(
  async () => await circunscricao({ id: 1, nome: 'Brasília', sigla: 'BSB' })
)
