import { papelAcesso } from "Database/factories/corporativo/PapelAcessoFactory"
import { cache } from "../SamplesUtils"
import { app } from "./SistemaSamples"

export const chefe = cache(
  async () => await papelAcesso({nome: "chefe", sistema: await app()})
)

export const funcionario = cache(
  async () => await papelAcesso({nome: "funcionario", sistema: await app()})
)
