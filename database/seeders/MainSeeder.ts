import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import { appBaseData } from 'Database/samples/AppSamples';
import { corporativoBaseData } from 'Database/samples/corporativo/CorporativoSamples';

export default class MainSeeder extends BaseSeeder {
  public async run() {
    await corporativoBaseData();
    await appBaseData();
  }
}
