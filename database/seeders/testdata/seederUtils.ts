import type { DateFormatForTests } from "Database/factories/FactoryUtils";
import { DateTime } from "luxon";

export const dt = (data: DateFormatForTests) => {
  return DateTime.fromFormat(data, 'dd/MM/yyyy', { locale: 'pt-BR' } )
}
