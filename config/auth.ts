/**
 * Config source: https://git.io/JY0mp
 *
 * Feel free to let us know via PR, if you find something broken in this config
 * file.
 */

import type { AuthConfig } from '@ioc:Adonis/Addons/Auth'
import Env from '@ioc:Adonis/Core/Env'

/*
|--------------------------------------------------------------------------
| Authentication Mapping
|--------------------------------------------------------------------------
|
| List of available authentication mapping. You must first define them
| inside the `contracts/auth.ts` file before mentioning them here.
|
*/
const authConfig: AuthConfig = {
  guard: 'basicCustom',
  guards: {
    basicCustom: {
      driver: 'basicCustom',
      realm: 'Login',
      provider: {
        driver: 'ldap',
        connection: 'mpdft',
        ldapHost: Env.get('LDAP_HOST'),
        ldapAdminDn: Env.get('LDAP_ADMIN_DN'),
        ldapAdminPassword: Env.get('LDAP_ADMIN_PASSWORD'),
        ldapUserSearchBase: Env.get('LDAP_USER_SEARCH_BASE'),
        ldapUserAttribute: Env.get('LDAP_USER_ATTRIBUTE'),
      },
    },
    basic: {
      driver: 'basic',
      realm: 'Login',
      provider: {
        driver: 'ldap',
        connection: 'mpdft',
        ldapHost: Env.get('LDAP_HOST'),
        ldapAdminDn: Env.get('LDAP_ADMIN_DN'),
        ldapAdminPassword: Env.get('LDAP_ADMIN_PASSWORD'),
        ldapUserSearchBase: Env.get('LDAP_USER_SEARCH_BASE'),
        ldapUserAttribute: Env.get('LDAP_USER_ATTRIBUTE'),
      },
    },
  },
}

export default authConfig
