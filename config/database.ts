/**
 * Config source: https://git.io/JesV9
 *
 * Feel free to let us know via PR, if you find something broken in this config
 * file.
 */

 import type { DatabaseConfig } from '@ioc:Adonis/Lucid/Database'
 import Env from '@ioc:Adonis/Core/Env'
 
 const databaseConfig: DatabaseConfig = {
   connection: "app",
   connections: {
     app: {
       client: 'mssql',
       connection: {
         ...(Env.get("MSSQL_DOMAIN") && {type: "ntlm", domain: Env.get("MSSQL_DOMAIN")}),
         server: Env.get("MSSQL_SERVER") ,
         port: Env.get("MSSQL_PORT"),
         database:  Env.get("MSSQL_DATABASE") ,
         user: Env.get("MSSQL_APP_DB_USER"),
         password: Env.get("MSSQL_APP_DB_PASSWORD"),
         options: {
             ...(Env.get("MSSQL_INSTANCE") && {instanceName: Env.get("MSSQL_INSTANCE")}),        
             trustServerCertificate: true
         }
       },
       migrations: {
         naturalSort: true,
         tableName: 'MIGRATION_VERSION_CONTROL',
         paths: Env.get('RUN_LOCALHOST_MIGRATIONS')
           ? ['./database/migrations/localhost', './database/migrations/production']
           : ['./database/migrations/production'],
         disableRollbacksInProduction: true
       },
       healthCheck: true,
       debug: (Env.get('NODE_ENV') === 'localhost'),
     },
 
   },
 }
 export default databaseConfig
 