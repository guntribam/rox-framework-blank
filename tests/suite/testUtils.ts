import type { Table } from 'gauge-ts'
import axios from 'axios'

const baseUrl = `http://localhost:3333`
const baseApiUrl = `${baseUrl}/api`

export interface Token {
  token: string
}

export interface DomainProp {
  field: string
  domain: Array<any>
}

export interface EntryWithEntity {
  entry: any
  entity: object
}

export interface Dictionary {
  buildEntryWithEntity: (table: any) => any
  translate: (entry: object, field: string) => any,
  getAuth: (entry: object, field: string) => {username: string, password: string}
}

const buildEntryWithEntity = (table: Table, dictionary: object) => {
  // @ts-ignore
  const entries = [Object.fromEntries(table.getTableRows()[0]._cells)]
  return entries.map((entry) => {
    const entity = Object.entries(dictionary)
      .map(([key, value]: [string, DomainProp]) => {
        if (value?.field.length === 0) return undefined
        try {
          return {
            [value.field]: value.domain.find((v) => v[entry[key]] !== undefined)[entry[key]],
          }
        } catch (error) {
          throw new Error(`Erro na chave: "${key}" -> ${error}`);
        }
      })
      .filter((obj) => obj).reduce((a, b) => ({ ...a, ...b }), {})
    return {entry, entity}
  })
}

const translate = (entry: object, field: string, dictionary: string): any => {
  const result = dictionary[field].domain.find((d) => d[entry[field]])[entry[field]]
  return result
}

export const createDictionary = (dictionary: any): Dictionary => {
  return {
    buildEntryWithEntity: (table: any) => buildEntryWithEntity(table, dictionary),
    translate: (entry: object, field: string) => translate(entry, field, dictionary),
    getAuth: (entry: object, field: string) => {
      const login = translate(entry, field, dictionary)
      return {username: login, password: login};
    }
  }
}

export const initTestBaseState = async () => {
  await axios.get(`${baseUrl}/inittestbasestate`)
}

export const restoreTestBaseState = async () => {
  await axios.get(`${baseUrl}/restoretestbasestate`)
}

export const resetTestBaseState = async () => {
  await axios.get(`${baseUrl}/resettestbasestate`)
}
export const seedData = async (seederName: string) => {
  const {data} = await axios.post(`${baseUrl}/seed`, {seederName})
  return data;
}
interface POST {url: string, data: any, auth: {username: string, password: string}}

export const api = {
  post: async ({url, data, auth}: POST) => await axios.post(`${baseApiUrl}${url}`, data, {auth})
}
