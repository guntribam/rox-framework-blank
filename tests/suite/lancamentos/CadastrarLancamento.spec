# Cadastrar Lançamento
Tags: cadastrarLancamento

| Nº | Cenário                | Usuário    | Valor  | Descrição | Conta  | Tipo de Lançamento | Estado do Lançamento | data   | mensagem                                   |
|----|------------------------|------------|--------|-----------|--------|--------------------|----------------------|--------|--------------------------------------------|
| 1  | Usuário sem acesso     | sem acesso | válido | válido    | válido | válido             | válido               | válido | Acesso negado        |
| 2  | sem valor              | válido     | nenhum | válido    | válido | válido             | válido               | válido | O campo "Valor" é obrigatório              |
| 3  | sem descrição          | válido     | válido | nenhum    | válido | válido             | válido               | válido | O campo "Descrição" é obrigatório          |
| 4  | sem conta              | válido     | válido | válido    | nenhum | válido             | válido               | válido | O campo "Conta" é obrigatório          |
| 5  | sem tipo de lançamento | válido     | válido | válido    | válido | nenhum             | válido               | válido | O campo "Tipo de Lançamento" é obrigatório |
| 6  | sem data               | válido     | válido | válido    | válido | válido             | válido               | nenhum | O campo "data" é obrigatório               |
| 7  | Sucesso                | válido     | válido | válido    | válido | válido             | válido               | válido | Lançamento cadastrado com sucesso          |

## Cadastrar Lançamento

* Cadastrar Lançamento - <Nº> - <Cenário>
| Nº   | Usuário   | Valor   | Descrição   | Conta   | Tipo de Lançamento   | Estado do Lançamento   | data   | mensagem   |
|------|-----------|---------|-------------|---------|----------------------|------------------------|--------|------------|
| <Nº> | <Usuário> | <Valor> | <Descrição> | <Conta> | <Tipo de Lançamento> | <Estado do Lançamento> | <data> | <mensagem> |
