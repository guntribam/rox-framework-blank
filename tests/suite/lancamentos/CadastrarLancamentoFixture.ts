import assert from 'assert';
import { BeforeSpec, DataStoreFactory, Step, Table} from 'gauge-ts'
import { api, createDictionary, Dictionary, restoreTestBaseState, seedData } from '../testUtils'

export default class CadastrarLancamentoFixture{
  @BeforeSpec({tags: ["cadastrarLancamento"]})
  async beforeSpec() {
    await restoreTestBaseState();
    const domainInfo = await seedData("CadastrarLancamentoSeeder")
    DataStoreFactory.getSpecDataStore().put("dictionary", createDictionary(domainInfo))
  }

  @Step('Cadastrar Lançamento - <numero> - <cenario> <table>')
  async cadastrarLancamento(_, __, table: Table) {
    const dictionary: Dictionary = DataStoreFactory.getSpecDataStore().get("dictionary")
    for (const {entry, entity} of dictionary.buildEntryWithEntity(table)) {
        const auth = dictionary.getAuth(entry, "Usuário");
        try {
          await api.post({url: "/lancamentos", data: entity, auth})
        } catch ({response}) {
          assert.deepStrictEqual(entry.mensagem, response.data.errors[0].message)
        }
    }
  }
}
