import { AfterSuite, BeforeSuite } from "gauge-ts";
import { initTestBaseState, resetTestBaseState } from "./testUtils";


export default class CleanDatabaseFixture {
  @BeforeSuite()
  async beforeSuite(){
    await initTestBaseState()
  }
  @AfterSuite()
  async afterSuite(){
    await resetTestBaseState()
  }
}
